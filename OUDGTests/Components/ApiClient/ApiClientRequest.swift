//
//  ApiClientRequest.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation
import Alamofire
import Realm
import RealmSwift
import ObjectMapper

public class ApiClientRequest {
    internal var manager: Alamofire.Manager!
    
    public typealias RequestCompletion = (request: NSURLRequest?, response: NSHTTPURLResponse?, json: AnyObject?, error: NSError?) -> Void
    
    public typealias RequestMethodPOSTCompletion = (success: Bool, response: [String: AnyObject]?, error: NSError?) -> Void
    
    let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
    
    public init() {
        manager = Alamofire.Manager(configuration: configuration)
    }
    
    // MARK: Generic Request JSON Data
    
    /**
     Create a request using the custom manager for the specified method, URL string, parameters, parameter encoding.
     Response data serialized to JSON
     
     - parameter method: The HTTP method.
     - parameter URLString: The URL string.
     - parameter parameters: The request parameters.
     - parameter encoding: The parameters encoding. `.URL` by default.
     
     - parameter completion: Completion Handler with JSON object serialized.
     
     - returns: The created request.
     */
    public func requestWithUrl(url:String, method:Alamofire.Method, parameters: [String : AnyObject]? = nil, encoding: ParameterEncoding = .URL, headers: [String : String]? = nil, completion: RequestCompletion?) -> Request {
        
        // The headers is configured on maneger
        let request = self.manager.request(method, url, parameters: parameters, encoding: encoding, headers: headers).responseJSON { (response) -> Void in
            // Check if request is canceled
            if let code = response.result.error?.code where code == NSURLErrorCancelled {
                return
            }
            
            // Execute Block completion
            if let _block = completion {
                _block(request: response.request , response: response.response, json: response.result.value, error: response.result.error)
            }
        }
        
        return request
    }
    
    
    public func requestGET(url url:String, cacheDuration: CacheDuration, parameters: [String : AnyObject]? = nil, completion: RequestCompletion) -> Request? {
        // Cache (from)
        if cacheDuration != CacheDuration.zero, let json = OUDGApiClient.cacheManager.getDataWithPath(url) {
            completion(request: nil, response: nil, json: json, error: nil)
            return nil
        }
        
        var headers : [String: String]? = nil
        if let cache = OUDGApiClient.cacheManager.getCacheObject(url) {
            headers = ["Etag": cache.etag]
        }
        
        return self.requestMethod(url: url, method: .GET, parameters: parameters, headers: headers, completion: completion)
    }
    
    
    public func requestPOST(url url:String, parameters: [String : AnyObject]? = nil, completion: RequestCompletion) -> Request? {
        return self.requestWithUrl(url, method: .POST, parameters: parameters, completion: { (request, response, json, error) -> Void in
            
            guard self.acceptableStatusCode(ApiClientStatusCode.Ok, ApiClientStatusCode.Created, inResponse: response) else {
                let _error = error ?? ObjectSerializer.errorWithCode(-10, failureReason: "Invalid Status Code: \(response?.statusCode)")
                completion(request: nil, response: nil, json: nil, error: _error)
                return
            }
            
            completion(request: request, response: response, json: ApiClientParser.parseJSON(json), error: error)
            return
        })
    }
    
    
    public func requestDELETE(url url:String, parameters: [String : AnyObject]? = nil, completion: RequestCompletion) -> Request? {
        return self.requestMethod(url: url, method: .DELETE, parameters: parameters, completion: completion)
    }
    
    
    public func requestPUT(url url:String, parameters: [String : AnyObject]? = nil, completion: RequestCompletion) -> Request? {
        return self.requestMethod(url: url, method: .PUT, parameters: parameters, completion: completion)
    }
    
    
    // Auxiliar method for requests waiting status code 200
    private func requestMethod(url url:String, method: Alamofire.Method, parameters: [String : AnyObject]? = nil, headers: [String : String]? = nil, encoding: ParameterEncoding = .URL, completion: RequestCompletion) -> Request? {
        
        return self.requestWithUrl(url, method: method, parameters: parameters, encoding: .JSON, headers: headers, completion: { (request, response, json, error) -> Void in
            guard self.acceptableStatusCode(ApiClientStatusCode.Ok, inResponse: response) else {
                let _error = error ?? ObjectSerializer.errorWithCode(-10, failureReason: "Invalid Status Code: \(response?.statusCode)")
                
                completion(request: nil, response: nil, json: nil, error: _error)
                return
            }
            completion(request: request, response: response, json: ApiClientParser.parseJSON(json), error: error)
            return
        })
    }
    
    // StatusCode válidos para uma requisição (200, 201, ...)
    public func acceptableStatusCode(statusCodes: Int..., inResponse response: NSHTTPURLResponse?) -> Bool {
        
        guard let _response = response else {
            return false
        }
        
        for status in statusCodes {
            if _response.statusCode == status {
                return true
            }
        }
        return false
    }
}


// MARK: - Rest Mappable Object

public extension ApiClientRequest {
    
    // MARK: Request GET DICT
    public func requestGETObject<T: Mappable>(type: T.Type, url: String, cacheDuration: CacheDuration = CacheDuration.zero, sectionName: String = "", completionHandler: (result: T?, error: NSError?) -> Void) -> Request? {
        
        return self.requestGET(url: url, cacheDuration: cacheDuration, completion: { (request, response, json, error) -> Void in
            return self.parseJSONModel(type, url: url, request: request, json: json, error: error,  cacheDuration: cacheDuration, sectionName: sectionName, completionHandler: completionHandler)
        })
    }
    
    // MARK: Request GET ARRAY
    public func requestGETObjects<T: Mappable>(type: T.Type, url: String, cacheDuration: CacheDuration = CacheDuration.zero, sectionName: String = "", completionHandler: (results: [T]?, error: NSError?) -> Void) -> Request? {
        // Request server
        let request = self.requestGET(url: url,  cacheDuration: cacheDuration, completion: { (request, response, json, error) -> Void in
            
            // Caso tenhamos falha no request, retornamos um erro
            if let _error = error {
                completionHandler(results: nil, error: _error)
                return
            }
            
            var modelRest: [T]?
            do {
                modelRest = try ObjectSerializer.arrayObjects(T.self, fromArray: json as? [AnyObject])
                
                // Cache it - Verifica se tem dados e tempos(online|offline) válidos antes de salvar
                CacheManager.saveIfNeeded(T.self, requestUrl: url, sectionName: sectionName, data: json, cacheDuration: cacheDuration)
                
                completionHandler(results: modelRest, error: nil)
            } catch {
                let _error = ObjectSerializer.errorWithCode(-10, failureReason: "Unable to parse JSON")
                completionHandler(results: nil, error: _error)
                return
            }
        })
        
        return request
    }
    
    // Parse json data only for dictionary models
    internal func parseJSONModel<T: Mappable>(type: T.Type, url: String, request: NSURLRequest?, json: AnyObject?, error: NSError?, cacheDuration: CacheDuration, sectionName: String = "", completionHandler: (result: T?, error: NSError?) -> Void) {
        
        if let _error = error {
            completionHandler(result: nil, error: _error)
            return
        }
        
        var modelRest: T?
        do {
            modelRest = try ObjectSerializer.object(T.self, fromDictionary: json as? [String: AnyObject])
            
            // Cache it - Verifica se tem dados e tempos(online|offline) válidos antes de salvar
            CacheManager.saveIfNeeded(T.self, requestUrl: url, sectionName: sectionName, data: json, cacheDuration: cacheDuration)
            
            completionHandler(result: modelRest, error: nil)
            return
        } catch {
            let _error = ObjectSerializer.errorWithCode(-10, failureReason: "Unable to parse JSON")
            completionHandler(result: nil, error: _error)
            return
        }
    }
    
}

public extension ApiClientRequest {
    
    // MARK: Request GET ARRAY with HeadersResponse
    public func requestGETObjects<T: Mappable>(type: T.Type, url: String, cacheDuration: CacheDuration = CacheDuration.zero, sectionName: String = "", completionHandler: (results: [T]?, headersResponse: HeadersResponse?, error: NSError?) -> Void) -> Request? {
        // Request server
        let request = self.requestGET(url: url,  cacheDuration: cacheDuration, completion: { (request, response, json, error) -> Void in
            
            // Caso tenhamos falha no request, retornamos um erro
            if let _error = error {
                completionHandler(results: nil, headersResponse: nil, error: _error)
                return
            }
            
            var modelRest: [T]?
            do {
                modelRest = try ObjectSerializer.arrayObjects(T.self, fromArray: json as? [AnyObject])
                
                // Realiza o parse dos dados do Header Response
                let headersResponse = HeadersResponse.parseHeadersResponse(response?.allHeaderFields)
                
                // Cache it - Verifica se tem dados e tempos(online|offline) válidos antes de salvar
                CacheManager.saveIfNeeded(T.self, requestUrl: url, sectionName: sectionName, data: json, headersResponse: response?.allHeaderFields, cacheDuration: cacheDuration)
                
                completionHandler(results: modelRest, headersResponse: headersResponse, error: nil)
            } catch {
                let _error = ObjectSerializer.errorWithCode(-10, failureReason: "Unable to parse JSON")
                completionHandler(results: nil, headersResponse: nil, error: _error)
                return
            }
        })
        
        return request
    }
}
