//
//  InstanceFactory.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


struct InstanceFactory {
    
    static let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    static let navigationController = InstanceFactory.storyboard.instantiateViewControllerWithIdentifier("NavigationController") as! UINavigationController
}
