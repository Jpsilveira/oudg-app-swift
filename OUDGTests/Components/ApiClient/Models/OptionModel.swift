//
//  OptionsModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper


public class OptionModel : BaseModel {
    
    public typealias blockCompletion = (QuestionModel?, NSError?) -> Void
    
    public var optionId: Int = 0
    public var title: String = ""
    
    // MARK: - Mappable Protocol
    public override func mapping(map: Map) {
        optionId         <- map["id"]
        title            <- map["title"]
    }
}
