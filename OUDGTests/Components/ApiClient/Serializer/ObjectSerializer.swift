//
//  ObjectSerializer.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation
import ObjectMapper

public class ObjectSerializer {
    
    public static let DataSerializationFailed: Int = -6004
    

    public static func errorWithCode(code: Int, failureReason: String) -> NSError {
        let userInfo = [NSLocalizedFailureReasonErrorKey: failureReason]
        return NSError(domain: "GSATVoDApiClient", code: code, userInfo: userInfo)
    }
    
    
    public static func object<T: Mappable>(_: T.Type, fromDictionary dictionary: [String: AnyObject]?) throws -> T? {
        
        guard let dic = dictionary else {
            let failureReason = "Data could not be serialized. Input data was nil."
            throw self.errorWithCode(DataSerializationFailed, failureReason: failureReason)
        }
        
        if let parsedObject = Mapper<T>().map(dic) {
            return parsedObject
        }
        
        let failureReason = "ObjectMapper failed to serialize."
        throw self.errorWithCode(DataSerializationFailed, failureReason: failureReason)
    }
    
    public static func object<T: Mappable>(_: T.Type, fromArray array: [AnyObject]?) throws -> T? {
        
        guard let _array = array else {
            let failureReason = "Data could not be serialized. Input data was nil."
            throw self.errorWithCode(DataSerializationFailed, failureReason: failureReason)
        }
        
        if let parsedObject = Mapper<T>().map(_array) {
            return parsedObject
        }
        
        let failureReason = "ObjectMapper failed to serialize."
        throw self.errorWithCode(DataSerializationFailed, failureReason: failureReason)
    }
    
    
    public static func arrayObjects<T: Mappable>(_: T.Type, fromArray: [AnyObject]? ) throws -> [T]? {
        
        guard let array = fromArray else {
            let failureReason = "Data could not be serialized. Input data was nil."
            throw self.errorWithCode(DataSerializationFailed, failureReason: failureReason)
        }
        
        if let parsedObjects = Mapper<T>().mapArray(array) {
            return parsedObjects
        }
        
        let failureReason = "ObjectMapper failed to serialize."
        throw self.errorWithCode(DataSerializationFailed, failureReason: failureReason)
    }
    
}
