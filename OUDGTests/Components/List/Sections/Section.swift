//
//  Section.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


@objc protocol SectionDelegate: class {
    func reloadSection(section: Section)
    optional func didPressHeader(section: Section)
}


class Section: NSObject, ListHeaderDelegate, ListFooterDelegate {
    
    weak var delegate: SectionDelegate?
    
    var title: String = ""
    
    var headerItem: AnyObject?
    
    var headerItems = [AnyObject]()
    
    var items = [AnyObject]()
    
    var error: NSError? = nil
    
    init(title: String? = nil, items: [AnyObject]? = nil) {
        
        self.title = title ?? ""
        self.items = items ?? [AnyObject]()
    }
    
    func backgroundColor() -> UIColor {return UIColor.clearColor()}
    
    func headerView() -> ListHeader.Type {return ListHeader.self}
    
    func footerView() -> ListFooter.Type {return ListFooter.self}
    
    func headerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {return CGSizeZero}
    
    func footerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {return CGSizeZero}
    
    func collectionCell() -> ListCollectionViewCell.Type {return ListCollectionViewCell.self}
    
    func cellItemSize(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {return CGSize.zero}
    
    func cellLineSpace() -> CGFloat {return 0}
    
    func cellColumnSpace() -> CGFloat {return 0}
    
    func insetForSection() -> UIEdgeInsets {return UIEdgeInsetsZero}
    
    func requestItems() {}
    
    func didSelectItem(index: Int) {}
    
    func loadingCell() -> Bool {
        if items is [LoadingModel] {
            return true
        }
        return false
    }
}
