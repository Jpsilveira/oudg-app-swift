//
//  ListCollectionViewHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

@objc protocol ListHeaderDelegate: class {
    optional func didPressHeaderButton(sender: AnyObject)
    optional func didPressHeaderSecondButton(sender: AnyObject)
    optional func didPressHeaderReload(sender: AnyObject)
    optional func didPressHeaderRequest(item: AnyObject)
    optional func textHeaderButton() -> String
    optional func textHeaderSecondButton() -> String
}

public class ListHeader: UICollectionReusableView, UpdateConstraintsProtocol {
    
    @IBOutlet weak var button: UIButton?
    
    @IBOutlet weak var secondButton: UIButton?
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView?
    
    @IBOutlet weak var titleLabel: UILabel?
    
    @IBOutlet weak var buttonTitleLabel: UILabel?
    
    @IBOutlet weak var secondButtonTitleLabel: UILabel?
    
    weak var delegate: ListHeaderDelegate?
    
    var section: Section!
    
    var item: AnyObject? = nil
    
    var loading: Bool = true {
        
        didSet {
            if loading == true {
                setLoadingLayout()
            } else {
                setHiddenLoading()
            }
        }
    }
    
    override public func drawRect(rect: CGRect) {
        super.drawRect(rect)
        setNeedsUpdateSubviewsConstraints()
    }
    
    override public func layoutSubviews() {
        setNeedsUpdateSubviewsConstraints()
        super.layoutSubviews()
    }
    
    public override func prepareForReuse() {
        super.prepareForReuse()
        
        loading = true
        titleLabel?.text = nil
        titleLabel?.hidden = true
        buttonTitleLabel?.text = nil
        buttonTitleLabel?.hidden = true
        secondButtonTitleLabel?.text = nil
        secondButtonTitleLabel?.hidden = true
    }
    
    func configure(indexPath: NSIndexPath) {
        setTitleLabelFont(Device.IS_4_INCHES_OR_SMALLER() ? width * 0.08 : height * 0.4)
    }
    
    func setLoadingLayout() {
        loadingView?.startAnimating()
        loadingView?.hidden = false
    }
    
    func setHiddenLoading() {
        loadingView?.stopAnimating()
        loadingView?.hidden = true
    }
    
    func setTitleLabelFont(fontSize: CGFloat) {
        
        titleLabel?.text = section.title
        titleLabel?.hidden = false
        titleLabel?.font = titleLabel?.font.fontWithSize(fontSize)
    }
    
    @IBAction func button(sender: AnyObject) {
        delegate?.didPressHeaderButton?(sender)
    }
    
    @IBAction func secondButton(sender: AnyObject) {
        delegate?.didPressHeaderSecondButton?(sender)
    }
    
    @IBAction func buttonReload(sender: AnyObject) {
        loading = true
        delegate?.didPressHeaderReload?(sender)
    }
}
