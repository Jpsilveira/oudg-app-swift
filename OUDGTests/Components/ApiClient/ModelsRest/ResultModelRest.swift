//
//  ResultModelRest.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/6/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper


public class ResultModelRest: BaseModelRest {
    public var results: [ResultModel] = []
    public typealias blockCompletion = (ResultModelRest?, NSError?) -> Void
    
    // MARK: - Mappable Protocol
    override public func mapping(map: Map) {
        super.mapping(map)
        results <- map["results"]
    }
}
