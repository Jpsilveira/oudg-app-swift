//
//  QuestionModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper


public class QuestionModel : BaseModel {
    
    public typealias blockCompletion = (QuestionModel?, NSError?) -> Void
    
    public var questionId: Int = 0
    public var attributeId: Int = 0
    public var text: String = ""
    public var options: [OptionModel] = []
    
    // MARK: - Mappable Protocol
    public override func mapping(map: Map) {
        questionId       <- map["id"]
        text             <- map["text"]
        attributeId      <- map["attribute"]
        options          <- map["options"]
    }
}
