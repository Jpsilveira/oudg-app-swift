//
//  Extensions.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 8/2/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

internal extension UIView {
    
    func boundInside(superView: UIView) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-0-[subview]-0-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics:nil, views:["subview":self]))
        superView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-0-[subview]-0-|", options: NSLayoutFormatOptions.DirectionLeadingToTrailing, metrics:nil, views:["subview":self]))
    }
}

struct RegisteredClass {
    
    private (set) var cellClass: AnyClass? = nil
    private (set) var identifier: String!
    
    init(cellClass: AnyClass?, identifier: String) {
        self.cellClass = cellClass
        self.identifier = identifier
    }
}

struct RegisteredNib {
    
    private (set) var nib: UINib? = nil
    private (set) var identifier: String!
    
    init(nib: UINib?, identifier: String) {
        self.nib = nib
        self.identifier = identifier
    }
}
