//
//  TranslateSection.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/13/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


public struct TranslateKind {
    static let name = "Nome Tribal"
    static let text = "Texto Tribal"
}

// MARK : HEADER

class TranslateSection: Section {
    
    var kind: String?
    
    var textToTranslate: String?

    override func headerView() -> ListHeader.Type {
        
        if error != nil {
            return ErrorHeader.self
            
        } else if kind != nil {
            
            return TranslateHeader.self
        }
        return TranslateMenuHeader.self
    }
    
    override func headerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        return CGSize(width: collectionWidth, height: collectionHeight * 0.7)
    }
    
    override func footerView() -> ListFooter.Type {
        return TestFooter.self
    }
    
    override func footerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        
        return CGSize(width: collectionWidth, height: collectionHeight * 0.15)
    }
    
    func requestTranslate(text: String?) {
        
        guard let _text = text where !_text.isEmpty else {return}
        
        textToTranslate = _text
        
        OUDGApiClient.sharedInstance.getTranslator(_text) { (model, error) in
            
            self.error = error
            
            if let translator = model where error == nil {
                
                self.headerItem = translator
                
                UserModel.sharedInstance.name = translator.original
                UserModel.sharedInstance.tribalName = translator.translated
                
                let home = InstanceFactory.navigationController.viewControllers.filter({$0 is HomeViewController}).first as? HomeViewController
                home?.setUserName()
            
            } else {
                self.headerItem = nil
            }
            
            self.delegate?.reloadSection(self)
        }
    }
}

// MARK: Header Delegate

extension TranslateSection {
    
    func didPressHeaderRequest(item: AnyObject) {
        
        guard let text = item as? String else {return}
        
        if !UserModel.sharedInstance.name.isEmpty {
            
            UIAlertController.showAlert(
                title: "Nirmiriel disse...",
                message: "Você já possui um nome tribal!\n\nTem certeza que deseja alterá-lo?",
                cancelButtonTitle: "Não",
                confirmationButtonTitle: "Sim",
                dissmissBlock: {
                    self.requestTranslate(text)
                },
                cancelBlock: {
                    
                    self.delegate?.reloadSection(self)
                }
            )
            return
        }
        
        requestTranslate(text)
    }
    
    func didPressHeaderReload(sender: AnyObject) {
    
        requestTranslate(textToTranslate)
    }
    
    func textHeaderButton() -> String {
        return TranslateKind.name
    }
    
    func didPressHeaderButton(sender: AnyObject) {
        kind = TranslateKind.name
        delegate?.reloadSection(self)
    }
    
    func textHeaderSecondButton() -> String {
        return TranslateKind.text
    }
    
    func didPressHeaderSecondButton(sender: AnyObject) {
        kind = TranslateKind.text
        delegate?.reloadSection(self)
    }
}

// MARK : Footer

extension TranslateSection {
    
    func showFooterButton() -> Bool {
        
        if error != nil {
            
            return false
        }
        return true
    }
    
    func textFooterButton() -> String {
        
        if kind != nil {
            return ""
        }
        
        return "Voltar ao Menu"
    }
    
    func showFooterSecondButton() -> Bool {
        return false
    }
    
    func textFooterSecondButton() -> String {
        return ""
    }
    
    func didPressFooterButton(item: AnyObject?) -> Bool {
        
        if kind != nil {
            kind = nil
            headerItem = nil
            delegate?.reloadSection(self)
            return true
        }
        
        delegate?.didPressHeader?(self)
        return true
    }
}
