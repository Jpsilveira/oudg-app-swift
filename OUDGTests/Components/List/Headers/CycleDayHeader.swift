//
//  CycleDayHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/20/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class CycleDayHeader: ListHeader {
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var papyriImage: UIImageView!
    
    @IBOutlet var optionLabels: Array<UILabel>!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var date: String = ""
    
    var time: String = ""
    
    var cycle: CycleModel?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        date = ""
        time = ""
        papyriImage.hidden = true
        datePicker.hidden = true
        secondButton?.hidden = true
        button?.hidden = true
        
        for optionLabel in optionLabels {
            optionLabel.text = nil
            optionLabel.hidden = true
        }
    }
    
    override func configure(indexPath: NSIndexPath) {
        
        button?.hidden = false
        
        for optionLabel in optionLabels {
            optionLabel.font = optionLabel.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? height * 0.058 : height * 0.05)
        }
        
        datePicker.datePickerMode = UIDatePickerMode.Date

        setData()
    }
    
    func setData() {
        
        guard !UserModel.sharedInstance.birthday.isEmpty else {return}
        
        papyriImage.hidden = false
        optionLabels.first?.text = "CICLO INICIAL: \(UserModel.sharedInstance.newBirthday)"
        optionLabels[1].text = "HORÁRIO: \(UserModel.sharedInstance.birthHour)"
        optionLabels[2].text = "DIA DA SEMANA: \(UserModel.sharedInstance.weekDay)"
        optionLabels[3].text = "LOCAL DE NASCIMENTO: \(UserModel.sharedInstance.birthPlace)"
        optionLabels[4].text = "ESTRELA DO SEU DIA: \(UserModel.sharedInstance.birthStar)"
        optionLabels[5].text = "SIGNO: \(UserModel.sharedInstance.birthSign)"
        optionLabels.last?.text = "FASE DA LUA: \(UserModel.sharedInstance.birthMoonPhase)"
        
        for optionLabel in optionLabels {
            optionLabel.hidden = false
        }
    }
    
    @IBAction override func button(sender: AnyObject) {
        super.button(sender)
        
        if UserModel.sharedInstance.birthday.isEmpty {
           loadDatePicker()
        
        } else {
            UIAlertController.showAlert(
                title: "Galaniel disse...",
                message: "Você já informou sua data de nascimento anteriormente!\n\nTem certeza que deseja alterá-la?",
                cancelButtonTitle: "Não",
                confirmationButtonTitle: "Sim",
                dissmissBlock: {
                    
                    self.loadDatePicker()
                }
            )
        }
    }
    
    func loadDatePicker() {
        
        papyriImage.hidden = true
        for optionLabel in optionLabels {
            optionLabel.hidden = true
        }
        
        button?.hidden = true
        secondButton?.hidden = false
        datePicker.hidden = false
    }

    @IBAction override func secondButton(sender: AnyObject) {
        
        let calendar = NSCalendar.currentCalendar()
        let comp = calendar.components([.Hour, .Minute], fromDate: datePicker.date)
        
        if date.isEmpty {
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            date = dateFormatter.stringFromDate(datePicker.date)
        
        } else if time.isEmpty {
            time = "T\(comp.hour):\(comp.minute)"
        }

        if !date.isEmpty && !time.isEmpty {

            secondButton?.hidden = true
            datePicker.hidden = true
            super.secondButton("\(date)\(time)")
        
        } else {
            datePicker.datePickerMode = UIDatePickerMode.Time
        }
    }
}
