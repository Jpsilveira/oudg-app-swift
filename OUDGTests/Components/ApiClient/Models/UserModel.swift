//
//  UserModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation

struct UserKind {
    
    static let user = "user"
    static let name = "userKind.name"
    static let tribalName = "userKind.tribalName"
    static let birthday = "userKind.birthday"
    static let newBirthday = "userKind.newBirthday"
    static let birthHour = "userKind.birthHour"
    static let weekDay = "userKind.weekDay"
    static let birthPlace = "userKind.birthPlace"
    static let birthStar = "userKind.birthStar"
    static let birthSign = "userKind.birthSign"
    static let birthMoonPhase = "userKind.birthMoonPhase"
    static let casteName = "userKind.casteName"
    static let casteImage = "userKind.casteImage"
    static let casteDescription = "userKind.casteDescription"
}

public class UserModel: NSObject {
    
    static public let sharedInstance = UserModel()
    
    public var name: String {
        
        get {
            return UserModel.get(UserKind.name)
        }
        set {
            UserModel.setObject(UserKind.name, object: newValue)
        }
    }
    
    public var tribalName: String {
        
        get {
            return UserModel.get(UserKind.tribalName)
        }
        set {
            UserModel.setObject(UserKind.tribalName, object: newValue)
        }
    }
    
    public var birthday: String {
        
        get {
            return UserModel.get(UserKind.birthday)
        }
        set {
            UserModel.setObject(UserKind.birthday, object: newValue)
        }
    }
    
    public var newBirthday: String {
        
        get {
            return UserModel.get(UserKind.newBirthday)
        }
        set {
            UserModel.setObject(UserKind.newBirthday, object: newValue)
        }
    }
    
    public var birthHour: String {
        
        get {
            return UserModel.get(UserKind.birthHour)
        }
        set {
            UserModel.setObject(UserKind.birthHour, object: newValue)
        }
    }
    
    public var weekDay: String {
        
        get {
            return UserModel.get(UserKind.weekDay)
        }
        set {
            UserModel.setObject(UserKind.weekDay, object: newValue)
        }
    }
    
    public var birthPlace: String {
        
        get {
            return UserModel.get(UserKind.birthPlace)
        }
        set {
            UserModel.setObject(UserKind.birthPlace, object: newValue)
        }
    }
    
    public var birthStar: String {
        
        get {
            return UserModel.get(UserKind.birthStar)
        }
        set {
            UserModel.setObject(UserKind.birthStar, object: newValue)
        }
    }
    
    public var birthSign: String {
        
        get {
            return UserModel.get(UserKind.birthSign)
        }
        set {
            UserModel.setObject(UserKind.birthSign, object: newValue)
        }
    }
    
    public var birthMoonPhase: String {
        
        get {
            return UserModel.get(UserKind.birthMoonPhase)
        }
        set {
            UserModel.setObject(UserKind.birthMoonPhase, object: newValue)
        }
    }
    
    public var casteName: String {
        
        get {
            return UserModel.get(UserKind.casteName)
        }
        set {
            UserModel.setObject(UserKind.casteName, object: newValue)
        }
    }
    
    public var casteImage: String {
        
        get {
            return UserModel.get(UserKind.casteImage)
        }
        set {
            UserModel.setObject(UserKind.casteImage, object: newValue)
        }
    }
    
    public var casteDescription: String {
        
        get {
            return UserModel.get(UserKind.casteDescription)
        }
        set {
            UserModel.setObject(UserKind.casteDescription, object: newValue)
        }
    }
    
    private class func setObject(key: String, object: String) {
        
        if !object.isEmpty {
            UserModel.set(key, object: object)
        } else {
            UserModel.clear(key)
        }
    }
    
    private class func get(key: String) -> String {
        return NSUserDefaults.standardUserDefaults().objectForKey(key) as? String ?? ""
    }
    
    private class func set(key: String, object: String) {
        NSUserDefaults.standardUserDefaults().setObject(object, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    private class func clear(key: String) {
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}
