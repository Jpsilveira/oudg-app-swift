//
//  ApiClientParser.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import SwiftyJSON

/**
    ApiClientParser

    Some functions that helping to parse the JSON objects.
*/
public class ApiClientParser {
    static let defaultJsonResultsKey: String  = "results"
    
    // MARK: - String Encoding
    static func encodingStringToUTF8(value: String) -> String {
        
        return value.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet()) ?? ""
    }
    
    //Optional [String : AnyObject]
    static public func dictionaryObject(object: AnyObject?) -> [String : AnyObject]? {
        return objectOfType(object, type: [String : AnyObject].self)
    }
    
    // Optional [AnyObject]
    static public func arrayObject(object: AnyObject?) -> [AnyObject]? {
        return objectOfType(object, type: [AnyObject].self)
    }
    
    static public func parseJSON(object: AnyObject?) -> AnyObject?  {
        if let arrayObject = object as? NSArray {
            return objectOfType(arrayObject, type: [AnyObject].self)
        }
        return objectOfType(object, type: [String : AnyObject].self)
    }
    
    // Transform object to specified type or nil
    static func objectOfType<T>(object: AnyObject?, type: T.Type) -> T? {
        
        guard let _obj = object as? T else {
            return nil
        }
        
        return _obj
    }
    
}
