//
//  HomeHeaderSection.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/28/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


struct MenuKind {
    static var caste = "Sua Casta"
    static var tribal = "Tradutor Tribal"
    static var cicle = "Registro de Nascimento"
    static var dice = "Jogo de Dados"
    static var nightCap = "Saidera Taverna Afiado"
    static var knowledge = "Teste de Conhecimentos"
}

class MenuSection: Section {
    
    var soon: Bool = false
    
    init(title: String? = nil, items: [AnyObject]? = nil, soon: Bool? = false) {
        super.init(title: title, items: items)
        
        self.soon = soon ?? false
    }
    
    override func headerView() -> ListHeader.Type {
        return MenuHeader.self
    }
    
    override func headerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        
        let aspectRatio: CGFloat = 405 / 75
        let height: CGFloat = (collectionWidth / aspectRatio)
        return CGSize(width: collectionWidth, height: height)
    }
}

// MARK : HEADER EVENTS

extension MenuSection {
    
    func didPressHeaderButton(item: AnyObject) {
        delegate?.didPressHeader?(self)
    }
}
