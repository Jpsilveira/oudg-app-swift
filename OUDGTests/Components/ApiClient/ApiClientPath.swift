//
//  ApiClient.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation

// MARK: - Url's
public struct ApiClientPath {
    static let quiz = "quiz"
    static let result = "\(quiz)/result"
    static let translator = "translator"
    static let cycle = "cycle"
    
}
