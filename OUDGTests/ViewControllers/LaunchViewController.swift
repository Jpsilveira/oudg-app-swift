//
//  LaunchViewController.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class LaunchViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {
            
            myAppDelegate.window?.rootViewController = InstanceFactory.navigationController
        })
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
