//
//  ApiClientSettigns.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation

// MARK: - Plist Keys
internal struct ApiClientSettigns {
    
    // Main Dic Key
    static let VOD_API_CLIENT            = "VOD_API_CLIENT"
    static let REQUEST_TIMEOUT_KEY       = "REQUEST_TIMEOUT"

    
    // Info (dic)
    static var plistFileName: String = "Info"
    static var info: NSDictionary? {
        return Plist.getAnyObject(VOD_API_CLIENT, plist_filename: plistFileName) as? NSDictionary
    }
    
    static func objectForKey(key: String) -> AnyObject? {
        return self.info?.objectForKey(key)
    }
}
