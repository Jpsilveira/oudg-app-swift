//
//  LoadingModel
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


// Classe utilizada para indicar que existe um modelo do collectionViewCell para Loading
class LoadingModel: NSObject {
    
}
