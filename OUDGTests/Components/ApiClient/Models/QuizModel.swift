//
//  AvailableOffilineModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper


public class QuizModel : BaseModel {
    
    public typealias blockCompletion = (QuizModel?, NSError?) -> Void
    
    public var quizId: Int = 0
    public var title: String = ""
    public var questions: [QuestionModel] = []

    // MARK: - Mappable Protocol
    public override func mapping(map: Map) {
        quizId       <- map["id"]
        title        <- map["title"]
        questions    <- map["questions"]
    }
}
