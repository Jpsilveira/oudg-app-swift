//
//  UIAlertController+Extension.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


extension UIAlertController {
    
    func show(viewController: UIViewController? = nil) {
        
        let topViewController = viewController ?? UIApplication.sharedApplication().delegate?.window?!.rootViewController
        
        topViewController?.presentViewController(self, animated: true, completion: nil)
    }
    
    static func showAlert(title title: String, message: String, cancelButtonTitle: String? = nil, confirmationButtonTitle: String? = nil, viewController: UIViewController? = nil, dissmissBlock: (()-> Void)? = nil , cancelBlock: (()-> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        // Cancel Button
        if cancelButtonTitle != nil {
            alert.addAction(UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                cancelBlock?()
            }))
        }
        // Confirmation button
        if confirmationButtonTitle != nil {
            alert.addAction(UIAlertAction(title: confirmationButtonTitle, style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                dissmissBlock?()
            }))
        }
        
        // Show
        dispatch_async(dispatch_get_main_queue()) {
            alert.show(viewController)
        }
    }
}
