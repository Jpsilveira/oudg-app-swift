//
//  HomeViewController.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class HomeViewController: ListCollectionViewController {
    
    @IBOutlet weak var userButton: UIButton!
    
    @IBOutlet weak var casteImage: UIImageView!
    
    @IBOutlet var rightLayoutConstraint: NSLayoutConstraint!
    
    @IBOutlet var leftLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLayoutConstraint()
        
        setUserName()
        
        setCasteImage()
        
        createMenu()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
        
        if size.width < size.height {
            
            leftLayoutConstraint.constant = size.width / 9
            rightLayoutConstraint.constant = size.width / 9
        } else {
            
            leftLayoutConstraint.constant = size.width / 3.7
            rightLayoutConstraint.constant = size.width / 3.7
        }
        
        collectionView.reloadData()
    }
    
    func setLayoutConstraint() {
        
        if Device.isPad() && Device.isLandscapeOrientation() {
            
            landscapeConstraint()
        
        } else if Device.isPad() {
            
            portraitConstraint()
        }
    }
    
    func portraitConstraint() {
        
        leftLayoutConstraint.constant = view.width / 9
        rightLayoutConstraint.constant = view.width / 9
    }
    
    func landscapeConstraint() {
        
        leftLayoutConstraint.constant = view.width / 3.7
        rightLayoutConstraint.constant = view.width / 3.7
    }
    
    func createMenu() {
        setLayoutConstraint()
        sections = [
            MenuSection(title: MenuKind.caste),
            MenuSection(title: MenuKind.tribal),
            MenuSection(title: MenuKind.cicle),
            MenuSection(title: MenuKind.dice, soon: true),
            MenuSection(title: MenuKind.nightCap, soon: true),
            MenuSection(title: MenuKind.knowledge, soon: true)
        ]
    }
    
    func setUserName() {
        userButton.setTitle(UserModel.sharedInstance.name, forState: .Normal)
        userButton.titleLabel?.font = userButton.titleLabel?.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? view.width * 0.04 : view.height * 0.03)
    }
    
    func setCasteImage() {
        casteImage.sd_setImageWithURL(NSURL(string: UserModel.sharedInstance.casteImage))
    }
    
    @IBAction func userNameTapped(sender: AnyObject) {
        
        setLayoutConstraint()
        
        let section = TranslateSection()
        section.kind = TranslateKind.name
        
        sections = [
            TitleSection(title: MenuKind.tribal),
            section
        ]
    }
}

// MARK: SectionDelegate
extension HomeViewController {
    
    func didPressHeader(section: Section) {
        
        guard section is MenuSection else {
            
            createMenu()
            return
        }
        
        switch section.title {
        
        case MenuKind.caste:
            
            setLayoutConstraint()
            sections = [
                TitleSection(title: MenuKind.caste),
                CasteSection(items: [LoadingModel()])
            ]
            
        case MenuKind.tribal:
            
            setLayoutConstraint()
            sections = [
                TitleSection(title: MenuKind.tribal),
                TranslateSection()
            ]
            
        case MenuKind.cicle:
            
            setLayoutConstraint()
            sections = [
                TitleSection(title: MenuKind.cicle),
                CycleDaySection()
            ]
            
        case MenuKind.dice:
            print(MenuKind.dice)
            
        case MenuKind.nightCap:
            print(MenuKind.nightCap)
            
        case MenuKind.knowledge:
            print(MenuKind.knowledge)
            
        default:
            createMenu()
        }
    }
}
