//
//  CacheManager.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import ObjectMapper
import QHashString

/**
 * Realm Schema Version (IMPORTANT)
 * When happens any changes in CacheDuration Model, incremment in of :realmSchemaVersion
 * 
 ====== VERSION 1 =====
 Field addition - headersResponse: NSData
 
 */
public let realmSchemaVersion: UInt64 = 1


func ==(lhs: CacheDuration, rhs: CacheDuration) -> Bool {
    let areEqual = lhs.online == rhs.online &&
        lhs.offline == rhs.offline
    
    return areEqual
}

func !=(lhs: CacheDuration, rhs: CacheDuration) -> Bool {
    return !(lhs == rhs)
}

/**
 Object with cache duration in seconds(time interval).
 Append it in date
 */
public struct CacheDuration {
    
    /// Default cache duration (zero). Without cache.
    public static let zero = CacheDuration(online: 0.0, offline: 0.0)
    
    public private(set) var online: NSTimeInterval   = 0.0
    public private(set) var offline: NSTimeInterval  = 0.0
    
    public var hasOnlineCache: Bool { return online > 0 }
    public var hasOfflineCache: Bool { return offline > 0 }

    
    public init(online: NSTimeInterval, offline: NSTimeInterval) {
        self.online = online
        self.offline = offline
    }
    
    /// Generate online expiration date from Now
    public func onlineExpirationDate() -> NSDate {
        return getExpiresIn(seconds: online)
    }
    
    /// Generate offline expiration date from Now
    public func offlineExpirationDate() -> NSDate {
        return getExpiresIn(seconds: online)
    }
    
    private func getExpiresIn(seconds sec: NSTimeInterval) -> NSDate {
        return NSDate().dateByAddingTimeInterval(sec)
    }
}

// MARK: - String(Hash)

private extension String {
    var hashString: String { return self.q_sha512String() }
}


// MARK: - CacheManager

public struct CacheManager {
    
    /**
     Save only if the data is not nil and has online or offline cache times
     */
    static func saveIfNeeded<T: Mappable>(type: T.Type, requestUrl: String, sectionName: String, data: AnyObject?, headersResponse: AnyObject? = nil, cacheDuration: CacheDuration) {
        
        guard let _data = data else {
            return
        }
        
        if cacheDuration.hasOnlineCache || cacheDuration.hasOfflineCache {
            save(requestUrl: requestUrl, sectionName: sectionName, data: _data, headersResponse: headersResponse, cacheDuration: cacheDuration)
        }
    }

    
    private static func save(requestUrl requestUrl: String, sectionName: String, data: AnyObject, headersResponse: AnyObject? = nil, cacheDuration: CacheDuration) {
        
        let path = requestUrl.hashString
        
        let cache = Cache()
        cache.path = path
        cache.section = slugfy(sectionName)
        cache.version = bundleVersion()
        /* AnyObject(NSDicionary, NSArray...) to NSData */
        cache.data = NSKeyedArchiver.archivedDataWithRootObject(data)
        cache.onlineExpiresIn = cacheDuration.onlineExpirationDate()
        cache.offlineExpiresIn = cacheDuration.offlineExpirationDate()
        
        /* AnyObject(NSDicionary, NSArray...) to NSData */
        if let headers = headersResponse
            where headers is [NSObject : AnyObject] {
            cache.headersResponse = NSKeyedArchiver.archivedDataWithRootObject(headers)
        }
        
        do {
            let realm = try Realm()
            try realm.write({ () -> Void in
                realm.add(cache, update: true)
            })
            
        } catch {
            print("Failure \(#function)\nError: \(error)")
        }
    }
    
    /**
    Returns the cache data, if exists.
    
    :param: path The request path (String)
    
    :returns: A Dictionary of data ([String: AnyObject]?)
    */
    static func getDataWithPath(path: String) -> [String: AnyObject]? {
        /* NSData to AnyObject(NSDicionary, NSArry...) */
        if let cache = getCacheObject(path), let unarchivedData = NSKeyedUnarchiver.unarchiveObjectWithData(cache.data) {
            return JSON(unarchivedData).dictionaryObject
        }
        return nil
    }
    
    /**
     Returns the cache data, if exists.
     
     :param: path The request path (String)
     
     :returns: A Dictionary of data ([String: AnyObject]?)
     */
    static func getCacheObject(path: String) -> Cache? {
        do {
            let realm = try Realm()
            
            guard let cache = realm.objects(Cache).filter("path = '\(path.hashString)'").first else {
                return nil
            }
            
            if cache.invalidated {
                print("CacheManager.getCacheObject - INVALIDATED")
                return nil
            }
            
            if (cache.isValidOnline() || (!OUDGConnection.hasConnection() && cache.isValidOffline())) {
                return cache
            }

        } catch {
            return nil
        }
        
        return nil
    }
    
    /**
    Deletes the cache objects filtered by section.
    
    :param: section The section name (String)
    */
    public static func deleteWithSection(section: String) -> Void {

        do {
            let realm = try Realm()
            
            try realm.write({ () -> Void in
                
                var objects: [Object] = []
                
                for o in realm.objects(Cache).filter("section = '\(slugfy(section))'") where !(o as Object).invalidated
                {
                    objects.append(o)
                }
                
                if objects.isEmpty {
                    return
                }
                
                realm.delete(objects)
//                print("Success (CacheManager.deleteWithSection)")
            })
            
            
        } catch {
            print("Failure (CacheManager.deleteWithSection)")
        }
        
    }
    
    /**
    Deletes the cache objects filtered by version.
    
    :param: exclude The only version that maintains the objects (String)
    */
    public static func deleteAllByVersion(excludeVersion version: String) -> Void {
        do {
            let realm = try Realm()
            
            try realm.write({ () -> Void in
                
                var objects: [Object] = []
                
                for o in realm.objects(Cache).filter("version != '\(version)'") where !(o as Object).invalidated
                {
                    objects.append(o)
                }
                
                if objects.isEmpty {
                    return
                }
                
                realm.delete(objects)
//                print("Success (CacheManager.deleteAllByVersion)")
            })
            
        } catch {
            print("Failure (CacheManager.deleteAllByVersion): \(error)")
        }
    }
    
    /**
    Deletes all cache objects.
    */
    public static func deleteAll() -> Void {
        
        do {
            let realm = try Realm()
            
            try realm.write({ () -> Void in
                realm.deleteAll()
//                print("Success (CacheManager.deleteAll)")
            })
            
        } catch {
            print("Failure (CacheManager.deleteAll): \(error)")
        }
    }
    
    public static func dropDatabase() {
        do {
            if let path = Realm.Configuration.defaultConfiguration.fileURL?.absoluteString {
                try NSFileManager.defaultManager().removeItemAtPath(path)
            }
        } catch _ {}
    }
    
    /**
     *  Migrate and update all cache objects.
     *  Always call in (application: , didFinishLaunchingWithOptions) to prevent any migration error.
     */
    public static func migrateRealmDB() {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: realmSchemaVersion,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
    }
}
