//
//  TranslateModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper


public class TranslateModel : BaseModel {
    
    public typealias blockCompletion = (TranslateModel?, NSError?) -> Void
    
    public var original: String = ""
    public var translated: String = ""
    
    // MARK: - Mappable Protocol
    public override func mapping(map: Map) {
        original         <- map["original"]
        translated       <- map["translated"]
    }
}
