//
//  ResultModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper


public class ResultModel : BaseModel {
    
    public var resultId: Int = 0
    public var title: String = ""
    public var description: String = ""
    public var quiz: Int = 0
    public var image: String = ""
    
    
    // MARK: - Mappable Protocol
    public override func mapping(map: Map) {
        resultId         <- map["id"]
        title            <- map["title"]
        description      <- map["description"]
        quiz             <- map["quiz"]
        image             <- map["image"]
    }
}
