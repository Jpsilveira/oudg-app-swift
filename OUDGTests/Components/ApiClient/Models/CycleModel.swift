//
//  CycleModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/22/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper


public class CycleModel : BaseModel {
    
    public typealias blockCompletion = (CycleModel?, NSError?) -> Void
    
    public var weekDay: String = ""
    public var month: String = ""
    public var year: Int = 0
    public var day: Int = 0
    public var hour: String = ""
    public var star: String = ""
    public var sign: String = ""
    public var city: String = ""
    public var moonPhase: String = ""
    
    // MARK: - Mappable Protocol
    public override func mapping(map: Map) {
        weekDay         <- map["week_day"]
        month           <- map["month"]
        year            <- map["year"]
        hour            <- map["hour"]
        star            <- map["star"]
        sign            <- map["sign"]
        city            <- map["city"]
        moonPhase            <- map["moon_phase"]
    }
}
