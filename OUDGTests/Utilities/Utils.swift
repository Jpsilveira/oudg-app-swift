//
//  Utils.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ReachabilitySwift


public class OUDGConnection {

    static let reachability: Reachability? = OUDGConnection.reachabilityInstance()
    
    public class func reachabilityInstance() -> Reachability? {
        
        let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            return nil
        }
        
        return reachability
    }
    
    public class func hasConnection() -> Bool {
        
        if let reachability = reachability {
            return reachability.isReachable()
        }
        
        return false
    }
}

// MARK: - Slugfy

public func slugfy(value:String) ->String{
    
    let array = ["'", "/", "\"", "`",",", ";", ":", ".", "?", "!","%", ")", "(", " "];
    
    let myStringAsciiData = value.lowercaseString.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)
    var myStringNoAccents = NSString(data: myStringAsciiData!, encoding: NSUTF8StringEncoding) as! String
    
    myStringNoAccents =  myStringNoAccents.stringByReplacingOccurrencesOfString("&", withString: "e")
    myStringNoAccents =  myStringNoAccents.stringByReplacingOccurrencesOfString("+", withString: "")
    
    for char in array {
        myStringNoAccents =  myStringNoAccents.stringByReplacingOccurrencesOfString(char, withString: "-")
    }
    
    while myStringNoAccents.containsString("--") {
        myStringNoAccents = myStringNoAccents.stringByReplacingOccurrencesOfString("--", withString: "-")
    }
    
    if (myStringNoAccents.characters.last == "-") {
        myStringNoAccents = myStringNoAccents.substringToIndex(myStringNoAccents.endIndex.predecessor())
    }
    
    return myStringNoAccents
}

/**
 Returns the bundle version
 
 - returns: The bundle version string (String)
 */
public func bundleVersion() -> String {
    if let _bundleVersion = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String {
        return _bundleVersion
    }
    
    return ""
}


public struct Device {
    
    public  static var currentDevice: UIDevice {
        struct Singleton {
            static let device = UIDevice.currentDevice()
        }
        return Singleton.device
    }
    
    public  static func isPhone() -> Bool {
        return currentDevice.userInterfaceIdiom == .Phone
    }
    
    public static func isPad() -> Bool {
        return currentDevice.userInterfaceIdiom == .Pad
    }
    
    public static var currentDeviceHeight: CGFloat {
        return UIScreen.mainScreen().bounds.size.height
    }
    
    public static var currentDeviceWidth: CGFloat {
        return UIScreen.mainScreen().bounds.size.width
    }
    
    public static var orientation: UIDeviceOrientation {
        return UIDevice.currentDevice().orientation
    }
    
    public static func isPortraitOrientation() -> Bool {
        let orientation: UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        return UIInterfaceOrientationIsPortrait(orientation)
    }
    
    public static func isLandscapeOrientation() -> Bool {
        return !self.isPortraitOrientation()
    }
    
    // MARK: - Device Size Checks
    
    public  enum Heights: CGFloat {
        case Inches_3_5 = 480
        case Inches_4 = 568
        case Inches_4_7 = 667
        case Inches_5_5 = 736
    }
    
    public  static func isSize(height: Heights) -> Bool {
        return currentDeviceHeight == height.rawValue
    }
    
    public static func isSizeOrLarger(height: Heights) -> Bool {
        return currentDeviceHeight >= height.rawValue
    }
    
    public  static func isSizeOrSmaller(height: Heights) -> Bool {
        return currentDeviceHeight <= height.rawValue
    }
    
    // MARK: Retina Check
    
    public  static func IS_RETINA() -> Bool {
        return (UIScreen.mainScreen().scale > 1.0)
    }
    
    // MARK: 3.5 Inch Checks
    
    public static func IS_3_5_INCHES() -> Bool {
        return isPhone() && isSize(.Inches_3_5)
    }
    
    public static func IS_3_5_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(.Inches_3_5)
    }
    
    public static func IS_3_5_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrSmaller(.Inches_3_5)
    }
    
    // MARK: 4 Inch Checks
    
    public static func IS_4_INCHES() -> Bool {
        return isPhone() && isSize(.Inches_4)
    }
    
    public static func IS_4_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(.Inches_4)
    }
    
    public static func IS_4_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrSmaller(.Inches_4)
    }
    
    // MARK: 4.7 Inch Checks
    
    public  static func IS_4_7_INCHES() -> Bool {
        return isPhone() && isSize(.Inches_4_7)
    }
    
    public  static func IS_4_7_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(.Inches_4_7)
    }
    
    public static func IS_4_7_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrLarger(.Inches_4_7)
    }
    
    // MARK: 5.5 Inch Checks
    
    public static func IS_5_5_INCHES() -> Bool {
        return isPhone() && isSize(.Inches_5_5)
    }
    
    public static func IS_5_5_INCHES_OR_LARGER() -> Bool {
        return isPhone() && isSizeOrLarger(.Inches_5_5)
    }
    
    public static func IS_5_5_INCHES_OR_SMALLER() -> Bool {
        return isPhone() && isSizeOrLarger(.Inches_5_5)
    }
}
