//
//  VHCollectionViewCell.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

internal class VHCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "VHCollectionViewCell"
    
    weak var collectionView: VHInternalCollectionView!
    
    var direction: UICollectionViewScrollDirection! {
        
        didSet {
            (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection = direction
        }
    }

    var collectionViewOffset: CGFloat {
        set {
            collectionView.contentOffset.x = newValue
        }
        
        get {
            return collectionView.contentOffset.x
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        collectionViewOffset = 0
        collectionView.sectionIndex = 0
    }
    
    override internal func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let _collectionView = VHInternalCollectionView()
        
        addSubview(_collectionView)
        
        collectionView = _collectionView
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false

        collectionView.boundInside(self)
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    internal func registerClasses(registerClasses: [RegisteredClass]) {
        
        for registerClass in registerClasses {
            collectionView.registerClass(registerClass.cellClass, forCellWithReuseIdentifier: registerClass.identifier)
        }
    }
    
    internal func registerNibs(registerNibs: [RegisteredNib]) {
        
        for registerNib in registerNibs {
            collectionView.registerNib(registerNib.nib, forCellWithReuseIdentifier: registerNib.identifier)
        }
    }
    
    internal func setCollectionViewDelegate<D: protocol<UICollectionViewDataSource, UICollectionViewDelegate>>(dataSourceDelegate: D) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
}
