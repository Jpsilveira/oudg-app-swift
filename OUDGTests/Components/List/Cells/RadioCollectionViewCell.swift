//
//  RadioCollectionViewCell.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/3/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

public let changeOption = "ChangeOption"

class RadioCollectionViewCell : ListCollectionViewCell {
    
    @IBOutlet weak var radioImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var optionId: Int!
    
    deinit {
        removeDidSelectOption()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        radioImage.image = UIImage(named: "radio_button_normal")
    }
    
    override func configure(model: AnyObject) {
        
        guard let optionModel = model as? OptionModel else {
            return
        }
        
        registerDidSelectOption()
        
        titleLabel.text = optionModel.title
        titleLabel.font = titleLabel?.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? height * 0.12 : height * 0.09)
        optionId = optionModel.optionId
        
        if let _casteSection = (section as? CasteSection), let _headerItem = section.headerItem as? QuestionModel {
            
            if let questionSelectedOption = _casteSection.answers["\(_headerItem.questionId)"] {
                changeRadioImage(questionSelectedOption)
            }
        }
    }
    
    override func didSelectCell() {
        
        if let _casteSection = (section as? CasteSection), let _headerItem = section.headerItem as? QuestionModel {
            
            _casteSection.answers["\(_headerItem.questionId)"] = optionId
        }
        
        postDidSelectOption(optionId)
    }
    
    func updateRadio(notification: NSNotification) {
        
        guard let notificationDict = notification.object as? [String:AnyObject], let _optionId = notificationDict["optionId"] as? Int else {return}
        
        changeRadioImage(_optionId)
    }
    
    func changeRadioImage(_optionId: Int) {
        
        if _optionId == optionId {
            
            radioImage.image = UIImage(named: "radio_button_selected")
        } else {
            
            radioImage.image = UIImage(named: "radio_button_normal")
        }
    }
    
    func registerDidSelectOption() {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(RadioCollectionViewCell.updateRadio(_:)), name: changeOption, object: nil)
    }
    
    func postDidSelectOption(_optionId: Int) {
        
        let notificationDict: [String:AnyObject] = ["optionId": optionId]
        NSNotificationCenter.defaultCenter().postNotificationName(changeOption, object: notificationDict)
    }
    
    func removeDidSelectOption() {
        
        NSNotificationCenter.defaultCenter().removeObserver( self, name: changeOption, object: nil)
    }
}
