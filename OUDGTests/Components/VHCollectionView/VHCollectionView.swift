//
//  VHCollectionViewController.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

@objc public protocol VHCollectionViewDelegate : class {
    

    optional func collectionView(collectionView: UICollectionView, directionForSection: Int) -> UICollectionViewScrollDirection
    
    optional func numberOfSections(collectionView: UICollectionView) -> Int
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    
    optional func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    
    optional func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath)
    
    optional func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath)

    
    // Layout
    optional func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets
    
    optional func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSection section: Int) -> CGFloat
    
    optional func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSection section: Int) -> CGFloat
    
    optional func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItem indexPath: NSIndexPath) -> CGSize
    
    optional func collectionView(collectionView: UICollectionView, viewForSupplementaryElement kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView
    
    optional func collectionView(collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, atIndexPath indexPath: NSIndexPath)
    
    optional func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooter section: Int) -> CGSize
    
    optional func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeader section: Int) -> CGSize
    
    optional func vhScrollViewDidScroll(scrollView: UIScrollView)
}

public class VHCollectionView: UICollectionView, UICollectionViewDelegate {
    
    public weak var vhDelegate: VHCollectionViewDelegate?
    
    private var registeredClasses = [RegisteredClass]()
    private var registeredNibs = [RegisteredNib]()
    
    
    var storedOffsets = [Int: CGFloat]()
    
    
    // MARK: - Initializers
    
    public init() {
        super.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
        self.commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }
    
    private func commonInit() {
        registerClass(VHCollectionViewCell.self, forCellWithReuseIdentifier:VHCollectionViewCell.reuseIdentifier)
        dataSource = self
        delegate = self
    }
    
    
    // MARK: - Private Methods
    
    private func distinctDirection(collectionView: UICollectionView, section: Int) -> UICollectionViewScrollDirection? {
        
        if let direction = vhDelegate?.collectionView?(collectionView, directionForSection: section) where (collectionView.collectionViewLayout as? UICollectionViewFlowLayout)?.scrollDirection != direction {
            
            return direction
        }
        
        return nil
    }
    
    // MARK - Dequeue Override
    
    public override func dequeueReusableCellWithReuseIdentifier(identifier: String, forIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let _ = distinctDirection(self, section: indexPath.section), let internalCollectionCell = self.cellForItemAtIndexPath(NSIndexPath(forRow: 0, inSection: indexPath.section)) as? VHCollectionViewCell {
            
            return internalCollectionCell.collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: NSIndexPath(forRow: indexPath.row, inSection: 0)) ?? UICollectionViewCell()
        }
        
        return super.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath)
    }
    
    // MARK - Private Methods
    
    private func getCell(collectionView collectionView: UICollectionView, forIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        guard let _ = vhDelegate, let cell = vhDelegate?.collectionView(collectionView, cellForItemAtIndexPath: indexPath) else {
            fatalError("implement func 'collectionView(collectionView, cellForItemAtIndexPath: indexPath)'")
        }
        
        return cell
    }
    
    
    public override func registerNib(nib: UINib?, forCellWithReuseIdentifier identifier: String) {
        super.registerNib(nib, forCellWithReuseIdentifier: identifier)
        
        registeredNibs.append(RegisteredNib(nib: nib, identifier: identifier))
    }
    
    
    public override func registerClass(cellClass: AnyClass?, forCellWithReuseIdentifier identifier: String) {
        super.registerClass(cellClass, forCellWithReuseIdentifier: identifier)
        
        guard  let _cellClass = cellClass else { return }
        
        if (NSStringFromClass(_cellClass) != NSStringFromClass(VHCollectionViewCell.self)) {
            registeredClasses.append(RegisteredClass(cellClass: cellClass, identifier: identifier))
        }
    }
    
    public override func registerNib(nib: UINib?, forSupplementaryViewOfKind kind: String, withReuseIdentifier identifier: String) {
        super.registerNib(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: identifier)
    }
}

extension VHCollectionView : UICollectionViewDataSource {
    
    public func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        if collectionView is VHInternalCollectionView {
            return 1
        }
        
        return vhDelegate?.numberOfSections?(collectionView) ?? 1
    }
    
    
    public func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if !(collectionView is VHInternalCollectionView), let _ = distinctDirection(collectionView, section: section) {
            return 1
        }
        
        var _section = section
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            _section = internalCollectionView.sectionIndex
        }
        
        guard let _ = vhDelegate, let numberOfItems = vhDelegate?.collectionView(collectionView, numberOfItemsInSection: _section) else {
            fatalError("implement func 'collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int)'")
        }
        
        return numberOfItems
    }
    
    
    public func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            let newIndexPath: NSIndexPath = NSIndexPath(forRow: indexPath.row, inSection: internalCollectionView.sectionIndex)
            return getCell(collectionView: self, forIndexPath: newIndexPath)
        }
        
        let cell = getCell(collectionView: collectionView, forIndexPath: indexPath)
        
        if !(collectionView is VHInternalCollectionView),
            let direction = distinctDirection(collectionView, section: indexPath.section),
            let _cell = collectionView.dequeueReusableCellWithReuseIdentifier(VHCollectionViewCell.reuseIdentifier, forIndexPath: indexPath) as? VHCollectionViewCell {
            
            _cell.direction = direction
            _cell.registerClasses(registeredClasses)
            _cell.registerNibs(registeredNibs)
            _cell.collectionView.backgroundColor = UIColor.clearColor()
            _cell.collectionView.sectionIndex = indexPath.section
            
            return _cell
        }
        
        return cell
    }
    
    
    public func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let _cell = cell as? VHCollectionViewCell {
            
            _cell.setCollectionViewDelegate(self)
            _cell.collectionViewOffset = storedOffsets[indexPath.section] ?? 0
            _cell.collectionView.sectionIndex = indexPath.section
            
        } else {
            
            var _section = indexPath.section
            if let internalCollectionView = collectionView as? VHInternalCollectionView {
                _section = internalCollectionView.sectionIndex
            }
            let newIndexPath = NSIndexPath(forItem: indexPath.item, inSection: _section)
            vhDelegate?.collectionView?(self, willDisplayCell: cell, forItemAtIndexPath: newIndexPath)
        }
    }
    
    
    public func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        if let _cell = cell as? VHCollectionViewCell {
            storedOffsets[indexPath.section] = _cell.collectionViewOffset
        }
    }
    
    public func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
               
        var _section = indexPath.section
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            _section = internalCollectionView.sectionIndex
        }
        let newIndexPath = NSIndexPath(forItem: indexPath.item, inSection: _section)
        vhDelegate?.collectionView!(self, didSelectItemAtIndexPath: newIndexPath)
    }
    
    public func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        var _section = indexPath.section
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            _section = internalCollectionView.sectionIndex
        }
        let newIndexPath = NSIndexPath(forItem: indexPath.item, inSection: _section)
        vhDelegate?.collectionView!(self, didDeselectItemAtIndexPath: newIndexPath)
    }
}

extension VHCollectionView : UICollectionViewDelegateFlowLayout {

    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        // Internal Collection
        
        // TODO: - Refatorar esse código
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            let newIndexPath: NSIndexPath = NSIndexPath(forRow: indexPath.row, inSection: internalCollectionView.sectionIndex)
            
            let collectionFlowLayout = vhDelegate?.collectionView?(self, layout: self.collectionViewLayout, sizeForItem: newIndexPath)
            return collectionFlowLayout ?? (self.collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize ?? CGSize.zero
        }
        
        
        let collectionFlowLayout = vhDelegate?.collectionView?(collectionView, layout: collectionViewLayout, sizeForItem: indexPath)
        
        if collectionView is VHCollectionView, let direction = distinctDirection(collectionView, section: indexPath.section) {
            
            let insetForSection = vhDelegate?.collectionView?(collectionView, layout: collectionViewLayout, insetForSection: indexPath.section) ?? (collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset ?? UIEdgeInsetsZero
            
            var height: CGFloat = 0
            var width: CGFloat = 0
            
            if direction == .Horizontal { // horizontal direction cell size
                width = collectionView.frame.width
                height = (collectionFlowLayout?.height ?? 0) + insetForSection.top + insetForSection.bottom
            
            } else { // vertical direction cell size
                width = (collectionFlowLayout?.width ?? 0) + insetForSection.left + insetForSection.right
                height = collectionView.frame.height
            }
            
            return CGSize(width: width, height: height)
        }
        
        return collectionFlowLayout ?? (collectionViewLayout as? UICollectionViewFlowLayout)?.itemSize ?? CGSize.zero
    }
    
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        if collectionView is VHCollectionView, let _ = distinctDirection(collectionView, section: section) {
            return UIEdgeInsetsZero
        }
        
        // TODO: - Refatorar
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            let value = vhDelegate?.collectionView?(self, layout: self.collectionViewLayout, insetForSection: internalCollectionView.sectionIndex) ?? (self.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset ?? UIEdgeInsetsZero
            return value
        }
        
        return vhDelegate?.collectionView?(collectionView, layout: collectionViewLayout, insetForSection: section) ?? (collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset ?? UIEdgeInsetsZero
    }
    
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        //TODO: Refatorar
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            return vhDelegate?.collectionView?(self, layout: self.collectionViewLayout, minimumLineSpacingForSection: internalCollectionView.sectionIndex) ?? (self.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing ?? 0
        }
        
        return vhDelegate?.collectionView?(collectionView, layout: collectionViewLayout, minimumLineSpacingForSection: section) ?? (collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing ?? 0
    }
    
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        
        // TODO: Refatorar
        if let internalCollectionView = collectionView as? VHInternalCollectionView {
            return vhDelegate?.collectionView?(self, layout: self.collectionViewLayout, minimumInteritemSpacingForSection: internalCollectionView.sectionIndex) ?? (self.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing ?? 0
        }
        
        return vhDelegate?.collectionView?(collectionView, layout: collectionViewLayout, minimumInteritemSpacingForSection: section) ?? (collectionViewLayout as? UICollectionViewFlowLayout)?.minimumInteritemSpacing ?? 0
    }
    
    
    public func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        return vhDelegate?.collectionView?(collectionView, viewForSupplementaryElement: kind, atIndexPath: indexPath) ?? UICollectionReusableView()
    }
    
    public func collectionView(collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, atIndexPath indexPath: NSIndexPath) {
        
        vhDelegate?.collectionView?(collectionView, didEndDisplayingSupplementaryView: view, forElementOfKind: elementKind, atIndexPath: indexPath)
    }
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if collectionView is VHInternalCollectionView {
            return CGSize.zero
        }
        
        return vhDelegate?.collectionView?(collectionView, layout: collectionViewLayout, referenceSizeForHeader: section) ?? CGSize.zero
    }
    
    public func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if collectionView is VHInternalCollectionView {
            return CGSize.zero
        }
        
        return vhDelegate?.collectionView?(collectionView, layout: collectionViewLayout, referenceSizeForFooter: section) ?? CGSize.zero
    }
    
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        vhDelegate?.vhScrollViewDidScroll?(scrollView)
    }
}
