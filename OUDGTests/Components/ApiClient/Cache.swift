//
//  Cache.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

public class Cache: Object {
    public dynamic var path = ""
    public dynamic var section = ""
    public dynamic var version = ""
    public dynamic var etag = ""
    public dynamic var onlineExpiresIn = NSDate()
    public dynamic var offlineExpiresIn = NSDate()
    public dynamic var data = NSData()
    public dynamic var headersResponse = NSData()
    
    // MARK: - Override methods
    
    override public class func primaryKey() -> String {
        return "path"
    }
    
    override public static func indexedProperties() -> [String] {
        return ["path", "version", "section", "etag"]
    }
    
    // Mark: Properties
    
    public func isValidOnline() -> Bool {
        let dateResult: NSComparisonResult = NSDate().compare(self.onlineExpiresIn)
        return (dateResult == NSComparisonResult.OrderedAscending)
    }
    
    public func isValidOffline() -> Bool {
        let dateResult: NSComparisonResult = NSDate().compare(self.offlineExpiresIn)
        return (dateResult == NSComparisonResult.OrderedAscending)
    }
}
