//
//  CycleDaySection.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/20/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


// MARK : HEADER

class CycleDaySection: Section {
    
    var kind: String?
    
    var date: String = ""
    
    override func headerView() -> ListHeader.Type {
        
        if error != nil {
            return ErrorHeader.self
        }
        return CycleDayHeader.self
    }
    
    override func headerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        
        return CGSize(width: collectionWidth, height: collectionHeight * 0.7)
    }
    
    override func footerView() -> ListFooter.Type {
        return TestFooter.self
    }
    
    override func footerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        
        return CGSize(width: collectionWidth, height: collectionHeight * 0.15)
    }
    
    func requestCycle(date: String) {
        
        UserModel.sharedInstance.birthday = date
        
        OUDGApiClient.sharedInstance.getCycleDay(date) { (model, error) in
            
            self.error = error
            
            if let cycle = model where error == nil {
                
                self.headerItem = cycle
                
                UserModel.sharedInstance.newBirthday = "\(cycle.day) de \(cycle.month) de \(cycle.year)"
                UserModel.sharedInstance.birthHour = cycle.hour
                UserModel.sharedInstance.weekDay = cycle.weekDay
                UserModel.sharedInstance.birthPlace = cycle.city
                UserModel.sharedInstance.birthStar = cycle.star
                UserModel.sharedInstance.birthSign = cycle.sign
                UserModel.sharedInstance.birthMoonPhase = cycle.moonPhase
                
            } else {
                self.headerItem = nil
            }
            
            self.delegate?.reloadSection(self)
        }
    }
}

// MARK: Header Delegate

extension CycleDaySection {

    func didPressHeaderSecondButton(sender: AnyObject) {
        
        date = (sender as? String) ?? ""
        
        guard !date.isEmpty else {return}
        
        requestCycle(date)
    }
    
    func didPressHeaderReload(sender: AnyObject) {
        
        requestCycle(date)
    }
}

// MARK : Footer

extension CycleDaySection {
    
    func showFooterButton() -> Bool {
        
        if error != nil {
            return false
        }
        return true
    }
    
    func textFooterButton() -> String {
        
        if kind != nil {
            return ""
        }
        
        return "Voltar ao Menu"
    }
    
    func showFooterSecondButton() -> Bool {
        return false
    }
    
    func textFooterSecondButton() -> String {
        return ""
    }
    
    func didPressFooterButton(item: AnyObject?) -> Bool {
        
        if kind != nil {
            kind = nil
            headerItem = nil
            delegate?.reloadSection(self)
            return true
        }
        
        delegate?.didPressHeader?(self)
        return true
    }
}
