//
//  ListCollectionViewCell.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class ListCollectionViewCell: UICollectionViewCell {
    
    var section: Section!
    
    var loading: Bool = true {
        
        didSet {
            if loading == true {
                setLoadingLayout()
            }
        }
    }
    
    func configure(model: AnyObject) {}
    
    func setLoadingLayout() {}
    
    func didSelectCell() {}
}
