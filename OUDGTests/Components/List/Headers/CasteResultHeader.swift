//
//  CasteResultHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/26/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

class CasteResultHeader: ListHeader {
    
    @IBOutlet weak var sealLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var sealImage: UIImageView!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        sealLabel.text = nil
        sealLabel?.hidden = true
        sealImage?.hidden = true
        descriptionLabel.hidden = true
        descriptionLabel.text = nil
        backgroundImage.hidden = true
    }
    
    override func configure(indexPath: NSIndexPath) {
        
        loading = false
        backgroundImage.hidden = false
        setSeal()
        setDescriptionLabel()
    }
    
    func setSeal() {
        
        sealLabel.hidden = false
        sealLabel.text = UserModel.sharedInstance.casteName.uppercaseString
        sealLabel.font = sealLabel.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? width * 0.04 : height * 0.05)
        
        sealImage.hidden = false
        sealImage.sd_setImageWithURL(NSURL(string: UserModel.sharedInstance.casteImage))
    }
    
    func setDescriptionLabel() {
        
        descriptionLabel.text = UserModel.sharedInstance.casteDescription
        descriptionLabel.hidden = false
        descriptionLabel.font = descriptionLabel?.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? width * 0.04 : height * 0.05)
    }
}
