//
//  VHInternalCollectionView.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 8/2/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

internal class VHInternalCollectionView: UICollectionView {
    
    internal var sectionIndex: Int = 0
    
    init() {
        super.init(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
