//
//  ApiVersionProtocol.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation

internal protocol ApiVersionProtocol {
    var apiVersion: String { get }
}
