//
//  HeaderCollectionViewHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/28/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class MenuHeader: ListHeader {
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        button?.setBackgroundImage(UIImage(named: "menu_button"), forState: .Normal)
        button?.setBackgroundImage(UIImage(named: "menu_button_selected"), forState: .Highlighted)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        button?.setBackgroundImage(UIImage(named: "menu_button"), forState: .Normal)
        button?.setBackgroundImage(UIImage(named: "menu_button_selected"), forState: .Highlighted)
    }
    
    override func configure(indexPath: NSIndexPath) {
        loading = false
        
        titleLabel?.hidden = false
        titleLabel?.text = section.title
        titleLabel?.font = titleLabel?.font.fontWithSize(height * 0.28)
        
        if let _section = section as? MenuSection where _section.soon == true {
            button?.setBackgroundImage(UIImage(named: "menu_button_soon"), forState: .Normal)
        }
    }

    @IBAction override func button(sender: AnyObject) {
        
        if loading == false && section.error == nil {
            delegate?.didPressHeaderButton?(sender)
        }
    }
}
