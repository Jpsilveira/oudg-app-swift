//
//  BaseModelRest.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation
import ObjectMapper


public class BaseModelRest: Mappable {
    public var count: Int = 0
    public var next: String? = nil
    public var previous: String? = nil
    
    // MARK: Initializers
    public init() {}
    public required init?(_ map: Map){}

    // MARK: Mappable Protocol
    public func mapping(map: Map) {
        count    <- map["count"]
        next     <- map["next"]
        previous <- map["previous"]
    }
    
    // MARK: - Date Transform
    internal class func dateTransform() -> DateFormatterTransform {
        return self.dateTransform(format: "yyyy-MM-dd'T'HH:mm:ss'Z'")
    }
    
    internal class func dateTransform(format format: String) -> DateFormatterTransform {
        return CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss'Z'")
    }

}
