//
//  UIView+Extensions.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        // FIXME: - Reveja o unwraping
        return NSStringFromClass(self).componentsSeparatedByString(".").last ?? String(Mirror(reflecting: self).subjectType)
    }
}

extension ListCollectionViewCell: ReusableView {}

extension ListHeader: ReusableView {}

extension ListFooter: ReusableView {}

extension UICollectionView {
    
    func reloadItemsInSection(section: Int, completion: (() -> Void)?) {
        
        self.performBatchUpdates({
            
            self.reloadSections( NSIndexSet(index: section) )
        
        }) { (finished) in
            
            completion?()
        }
    }
}
