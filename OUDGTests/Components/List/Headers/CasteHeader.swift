//
//  CasteHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/3/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import SDWebImage


class CasteHeader: ListHeader {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        descriptionLabel.hidden = true
        descriptionLabel.text = nil
    }
    
    override func configure(indexPath: NSIndexPath) {
        
        guard let question = section.headerItem as? QuestionModel where section.error == nil else {
            return
        }
        
        loading = false
        setDescriptionLabel(question)
    }

    func setDescriptionLabel(question: QuestionModel) {
        
        descriptionLabel.text = question.text
        descriptionLabel.hidden = false
        descriptionLabel.font = descriptionLabel?.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? width * 0.04 : height * 0.08)
    }
}
