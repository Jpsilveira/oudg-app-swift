//
//  Plist.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation

public class Plist : NSObject {
    static var DEFAULT_FILENAME = "Info"
    static let EXCEPTION_PLIST_NOT_FOUND_NAME = "PlistNotFoundException"
    static let EXCEPTION_PLIST_NOT_FOUND_MESSAGE = "Plist %@ not found in project"
    static let EXCEPTION_PROPERTY_NOT_FOUND_NAME = "ApiClientConfigException"
    static let EXCEPTION_PROPERTY_NOT_FOUND_MESSAGE = "Property %@ not found in %@.plist"
    
    /**
     Gets an instance of AnyObject type from the desired Plist file
     
     - parameter key: The key where the AnyObject was stored
     - parameter plist_filename: The Plist filename to search the key (Info.plist will be loaded if no param is provided)
     
     - returns: An AnyObject value
     */
    public static func getAnyObject(key: String, plist_filename: String = Plist.DEFAULT_FILENAME) -> AnyObject? {
        let dict: Dictionary<String, AnyObject> = Plist.load()!
        if let value: AnyObject = dict[key] {
            return value
        } else {
            print(
                NSString(
                    format: Plist.EXCEPTION_PROPERTY_NOT_FOUND_MESSAGE,
                    arguments: getVaList([key, plist_filename])
                )
            )
        }
        
        return nil
    }
    
    /**
     Gets an instance of a String from the desired Plist file
     
     - parameter key: The key where the String was stored
     - parameter plist_filename: The Plist filename to search the key (Info.plist will be loaded if no param is provided)
     
     - returns: A String value
     */
    public static func getString(key: String, plist_filename: String = Plist.DEFAULT_FILENAME) -> String! {
        return Plist.getAnyObject(key, plist_filename: plist_filename) as! String
    }
    
    /**
     Loads a Plist file
     
     - parameter filename: The Plist filename to search the key (Info.plist will be loaded if no param is provided)
     
     - returns: A dictionary with Plist's content
     */
    public static func load(filename: String = Plist.DEFAULT_FILENAME) -> Dictionary<String, AnyObject>? {
        if let path = NSBundle.mainBundle().pathForResource(filename, ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> {
                return dict
            }
        }
        else {
            NSException.raise(Plist.EXCEPTION_PLIST_NOT_FOUND_NAME,
                              format: Plist.EXCEPTION_PLIST_NOT_FOUND_MESSAGE,
                              arguments: getVaList([filename]))
        }
        
        return nil
    }
}
