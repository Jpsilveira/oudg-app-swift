//
//  TranslateMenuHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/20/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class TranslateMenuHeader: ListHeader {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        descriptionLabel.hidden = true
        descriptionLabel.text = nil
    }
    
    override func configure(indexPath: NSIndexPath) {
        
        setDescriptLabel()
        
        buttonTitleLabel?.hidden = false
        buttonTitleLabel?.font = buttonTitleLabel?.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? height * 0.08 : height * 0.05)
        buttonTitleLabel?.text = delegate?.textHeaderButton?()
        
        secondButtonTitleLabel?.hidden = false
        secondButtonTitleLabel?.font = secondButtonTitleLabel?.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? height * 0.08 : height * 0.05)
        secondButtonTitleLabel?.text = delegate?.textHeaderSecondButton?()
    }
    
    func setDescriptLabel() {
        
        descriptionLabel.text = "Nas tribos ao sul de Saupor, no Deserto de Mahar, vivem guerreiros estranhos, que falam em línguas antigas. Use o tradutor para se comunicar e fazer acordos comerciais com esse povo"
        descriptionLabel.hidden = false
        descriptionLabel.font = descriptionLabel?.font.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? height * 0.054 : height * 0.05)
    }
}
