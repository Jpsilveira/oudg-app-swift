//
//  BaseModel.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import ObjectMapper

public class BaseModel : Mappable {
    
    // MARK: Initializers
    public init() {}
    public required init?(_ map: Map){}
    
    // MARK: Mappable Protocol
    public func mapping(map: Map) {}
    
    // MARK: - Date Transform
    public class func dateTransform() -> DateFormatterTransform {
        return self.dateTransform(format: "yyyy-MM-dd'T'HH:mm:ss'Z'")
    }
    
    final public class func dateTransform(format format: String) -> DateFormatterTransform {
        return CustomDateFormatTransform(formatString: format)
    }
}
