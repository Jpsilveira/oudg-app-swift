//
//  Constants.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/23/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

let myAppDelegate:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
let urlBase = "http://api.oultimodosguardioes.com.br/api/"
let urlQuiz = "\(urlBase)quiz/"

func executeOnMainQueue(completion: () -> Void ) {
    if NSThread.isMainThread() {
        completion()
    } else {
        dispatch_async(dispatch_get_main_queue(), {
            completion()
        })
    }
}
