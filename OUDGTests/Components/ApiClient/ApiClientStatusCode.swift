//
//  ApiClientStatusCode.swift
//  OUDG
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

/**
    Request status code

    - Ok
    - Created
    - Accepted
    - NoContent
    - MovedPermanently
    - Found
    - NotModified
    - BadRequest
    - Unauthorized
    - Forbidden
    - NotFound
    - MethodNotAllowed
    - RequestTimeout
    - InternalServerError
    - NotImplemented
    - BadGateway
    - ServiceUnavailable
    - GatewayTimeout
*/
public struct ApiClientStatusCode {
    static let Ok = 200
    static let Created = 201
    static let Accepted = 202
    static let NoContent = 204
    static let MovedPermanently = 301
    static let Found = 302
    static let NotModified = 304
    static let BadRequest = 400
    static let Unauthorized = 401
    static let Forbidden = 403
    static let NotFound = 404
    static let MethodNotAllowed = 405
    static let RequestTimeout = 408
    static let InternalServerError = 500
    static let NotImplemented = 501
    static let BadGateway = 502
    static let ServiceUnavailable = 503
    static let GatewayTimeout = 504
}
