//
//  TestFooter.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/5/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class TestFooter: ListFooter {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var buttonLabel: UILabel!
    
    @IBOutlet weak var secondButtonLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        messageLabel.alpha = 0
        buttonLabel.hidden = true
        button?.hidden = true
        secondButtonLabel.hidden = true
        secondButton?.hidden = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        button?.setBackgroundImage(UIImage(named: "back_button"), forState: .Normal)
        button?.setBackgroundImage(UIImage(named: "back_button_selected"), forState: .Highlighted)
        
        secondButton?.setBackgroundImage(UIImage(named: "forward_button"), forState: .Normal)
        secondButton?.setBackgroundImage(UIImage(named: "forward_button_selected"), forState: .Highlighted)
    }
    
    override func configure(indexPath: NSIndexPath) {
        
        item = section.headerItem
        
        messageLabel?.font = messageLabel?.font.fontWithSize(height * 0.25)
        
        if delegate?.showFooterSecondButton?() == true, let textSecondButton = delegate?.textFooterSecondButton?() {
            
            secondButton?.hidden = false
            secondButtonLabel.hidden = false
            secondButtonLabel.text = textSecondButton
            secondButtonLabel?.font = messageLabel?.font.fontWithSize(height * 0.25)
        }
        
        if delegate?.showFooterButton?() == true, let textButton = delegate?.textFooterButton?() {
            
            button?.hidden = false
            buttonLabel.hidden = false
            buttonLabel.text = textButton
            buttonLabel?.font = messageLabel?.font.fontWithSize(height * 0.25)
        }
    }
    
    @IBAction override func secondButton(sender: AnyObject) {
        
        if delegate?.didPressFooterSecondButton?(item) == false {

            messageLabel.alpha  = 1
            buttonLabel.alpha = 0
            secondButtonLabel.alpha = 0
            
            UIView.animateWithDuration(3, delay: 0.0, options: .CurveEaseOut, animations: {
                
                self.messageLabel.alpha  = 0
                self.buttonLabel.alpha = 1
                self.secondButtonLabel.alpha = 1
                
            }, completion: nil)
        }
    }
}
