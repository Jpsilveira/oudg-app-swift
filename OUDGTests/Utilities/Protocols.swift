//
//  Protocols.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/5/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


protocol UpdateConstraintsProtocol {
    func setNeedsUpdateSubviewsConstraints()
}

extension UpdateConstraintsProtocol where Self: UIView {
    
    func setNeedsUpdateSubviewsConstraints() {
        let _ = self.subviews.map({
            $0.setNeedsUpdateConstraints()
        })
    }
}
