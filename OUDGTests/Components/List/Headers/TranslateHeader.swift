//
//  TranslateHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/13/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


class TranslateHeader: ListHeader {
    
    @IBOutlet weak var messageTextField: UITextView!
    
    @IBOutlet weak var resultTextField: UITextView!
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var obsLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        containerView.hidden = false
        resultTextField.hidden = true
        resultTextField.text = nil
        messageTextField.hidden = true
        messageTextField.text = nil
        obsLabel.hidden = true
    }
    
    override func configure(indexPath: NSIndexPath) {
        
        setObsFont()
        setMessageFont()
        setResultFont()
        
        addGestureTextView()
        
        guard (section as? TranslateSection)?.kind == TranslateKind.name else {return}
        
        if !UserModel.sharedInstance.name.isEmpty {
            messageTextField.text = UserModel.sharedInstance.name
        }
        
        if !UserModel.sharedInstance.tribalName.isEmpty {
            setResultFont(UserModel.sharedInstance.tribalName)
        }
    }
    
    func addGestureTextView() {
        
        let tapSubject: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TranslateHeader.dismissKeyboard))
        addGestureRecognizer(tapSubject)
    }
    
    func dismissKeyboard() {
        
        endEditing(true)
    }
    
    func setObsFont() {
        
        obsLabel.hidden = false
        obsLabel.font = obsLabel?.font?.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? height * 0.05 : height * 0.04)
    }
    
    func setMessageFont() {
        
        messageTextField.hidden = false
        messageTextField.text = (section as? TranslateSection)?.kind == TranslateKind.name ? "Seu Nome" : "Seu Texto"

        messageTextField.font = messageTextField?.font?.fontWithSize(Device.IS_4_INCHES_OR_SMALLER() ? height * 0.06 : height * 0.05)
    }
    
    func setResultFont(text: String? = nil) {
        
        if let _text = text where !_text.isEmpty {
            resultTextField.text = _text
        } else {
            resultTextField.text = (section as? TranslateSection)?.kind == TranslateKind.name ? "Nome Tribal" : "Texto Tribal"
        }
        
        resultTextField.hidden = false
        resultTextField.font = UIFont(name: "Papyrus", size: Device.IS_4_INCHES_OR_SMALLER() ? height * 0.06 : height * 0.05)
        resultTextField.textAlignment = .Center
    }
}


// MESSAGE
extension TranslateHeader : UITextViewDelegate {
    
    func textViewDidBeginEditing(textView: UITextView) {
        messageTextField.text = nil
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        
        if let text = textView.text where !text.isEmpty {
            delegate?.didPressHeaderRequest?(text)
        
        } else {
            setMessageFont()
            setResultFont()
        }
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
