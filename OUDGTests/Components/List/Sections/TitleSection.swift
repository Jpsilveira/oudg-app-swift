//
//  TitleHeader.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/26/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


// MARK : HEADER

class TitleSection: Section {
    
    override func headerView() -> ListHeader.Type {
        
        return ListHeader.self
    }
    
    override func headerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        
        return CGSize(width: collectionWidth, height: collectionHeight * 0.15)
    }
}
