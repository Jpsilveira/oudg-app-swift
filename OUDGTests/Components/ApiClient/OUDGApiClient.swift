//
//  XPApiClient.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

import Alamofire
import Realm
import RealmSwift
import SwiftyJSON
import ObjectMapper


final public class OUDGApiClient: ApiClientRequest {
    static public let sharedInstance = OUDGApiClient()
    
    private let validEndpoints = [ApiClientPath.quiz]
    
    // MARK: - Request Variables
    private var baseUrl: String!
    private var requestTimeout: NSTimeInterval = 15
    private var defaultPage: Int               = 1
    private var itemsPerPage: Int              = 20
    
    public static let cacheManager = CacheManager.self
    
    
    // MARK: - Override init
    
    override init() {
        super.init()
        
        setBaseUrl()
        setRequestTimeout()
        configuration.timeoutIntervalForRequest = requestTimeout
        manager = Alamofire.Manager(configuration: configuration)
    }
    
    
    // MARK: - Setters
    
    private func setBaseUrl() {
        baseUrl = "http://api.oultimodosguardioes.com.br/api"
    }
    
    private func setRequestTimeout() {
        if let timeout = ApiClientSettigns.objectForKey(ApiClientSettigns.REQUEST_TIMEOUT_KEY) as? NSTimeInterval {
            requestTimeout = timeout
        }
    }
    
    
    // MARK: - GET Quiz
    
    /**
     Returns a boolean with the result.
     
     :param: quizId               Quiz Id.
     :param: completionHandler   ((success: Bool, error: NSError?) -> ())
     
     :returns: Request?
     */
    public func getQuiz(quizId: Int, cacheDuration: CacheDuration = CacheDuration.zero, sectionName: String = "", completionHandler: QuizModel.blockCompletion) -> Request? {

        let url = assembleRequestUrl(ApiClientPath.quiz, id: quizId)
        return self.requestGETObject(QuizModel.self, url: url, cacheDuration: cacheDuration, sectionName: sectionName) { (modelRest, error) -> Void in
            completionHandler(modelRest, error)
        }
    }
    
    // MARK: - GET Quiz
    
    /**
     Returns a boolean with the result.
     
     :param: quizId               Quiz Id.
     :param: completionHandler   ((success: Bool, error: NSError?) -> ())
     
     :returns: Request?
     */
    public func getTranslator(text: String, cacheDuration: CacheDuration = CacheDuration.zero, sectionName: String = "", completionHandler: TranslateModel.blockCompletion) -> Request? {
        
        let queryString = "text=\(ApiClientParser.encodingStringToUTF8(text))"
        
        let url = assembleRequestUrl(ApiClientPath.translator, queryString: queryString)
        return self.requestGETObject(TranslateModel.self, url: url, cacheDuration: cacheDuration, sectionName: sectionName) { (modelRest, error) -> Void in
            completionHandler(modelRest, error)
        }
    }
    
    
    // MARK: - GET Quiz
    
    /**
     Returns a boolean with the result.
     
     :param: quizId               Quiz Id.
     :param: completionHandler   ((success: Bool, error: NSError?) -> ())
     
     :returns: Request?
     */
    public func getCycleDay(text: String, cacheDuration: CacheDuration = CacheDuration.zero, sectionName: String = "", completionHandler: CycleModel.blockCompletion) -> Request? {
        
        let queryString = "date=\(text)"
        
        let url = assembleRequestUrl(ApiClientPath.cycle, queryString: queryString)
        return self.requestGETObject(CycleModel.self, url: url, cacheDuration: cacheDuration, sectionName: sectionName) { (modelRest, error) -> Void in
            completionHandler(modelRest, error)
        }
    }
    
    
    // MARK: - Watch History
    
    /**
     Returns a boolean with the result.
     
     :param: quizId               Quiz Id.
     :param: optionIds            Array of optionIds [String].
     :param: completionHandler   ((success: Bool, error: NSError?) -> ())
     
     :returns: Request?
     */
    public func getResult(quizId: Int, answers: [String: Int], cacheDuration: CacheDuration = CacheDuration.zero, sectionName: String = "", completionHandler: ResultModelRest.blockCompletion) -> Request? {
        
        var url = ""
        var queryString = "quiz_id=\(quizId)&answers="
        
        do {
            let data = try NSJSONSerialization.dataWithJSONObject(answers, options: .PrettyPrinted)
            let jsonString = NSString(data: data, encoding: NSUTF8StringEncoding)
            let urlEncodedJson = jsonString!.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
            
            queryString += "\(urlEncodedJson!)"
            
            url = assembleRequestUrl(ApiClientPath.result, queryString: queryString)
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
        
        return self.requestGETObject(ResultModelRest.self, url: url, cacheDuration: cacheDuration, sectionName: sectionName) { (modelRest, error) -> Void in
            completionHandler(modelRest, error)
        }
    }
    
    // MARK: Request PUT
    public func requestMethodPUT(url url: String, parameters: [String: AnyObject]? = nil, completionHandler: (result: Bool, error: NSError?) -> Void) -> Request? {
        
        return self.requestPUT(url: url, parameters: parameters, completion: { (request, response, json, error) -> Void in
            var success: Bool = false
            if let _json = json as? [String: AnyObject], let _success = _json["success"] as? Bool where _success {success = true}
            completionHandler(result: success, error: error)
        })
    }
    
    // MARK: Request DELETE
    public func requestMethodDELETE(url url: String, parameters: [String: AnyObject]? = nil, completionHandler: (result: Bool, error: NSError?) -> Void) -> Request? {
        
        return self.requestDELETE(url: url, parameters: parameters, completion: { (request, response, json, error) -> Void in
            var success: Bool = false
            var _error: NSError?
            
            if let _json = json as? [String: AnyObject] {
                if let _success = _json["success"] as? Bool {
                    success = _success
                }
                
                if let _message = _json["message"] as? String {
                    let userInfo = [NSLocalizedFailureReasonErrorKey: _message]
                    _error = NSError(domain: "request Error", code: 1, userInfo: userInfo)
                }
            }
            completionHandler(result: success, error: _error)
        })
    }
    
    // MARK: Request POST
    
    public func requestMethodPOST(url url: String, parameters: [String: AnyObject]? = nil, completionHandler: ApiClientRequest.RequestMethodPOSTCompletion) -> Request? {
        
        return self.requestPOST(url: url, parameters: parameters, completion: { (request, response, json, error) -> Void in
            guard let _json = json as? [String: AnyObject], let success: Bool = _json["success"] as? Bool else {
                completionHandler(success: false, response: nil, error: error)
                return
            }
            completionHandler(success: success, response: _json, error: error)
        })
    }
    
    
    // MARK: - Assemble request
    private func assembleRequestUrl(path: String, id: Int? = nil, page: Int? = nil, queryString: String? = nil) -> String {
        var requestUrl = "\(baseUrl)/\(path)"
        
        if let _id = id { requestUrl += "/\(_id).json?"} else {requestUrl += ".json?"}
        if let _page = page { requestUrl += "&page=\(_page)" }
        if let _queryString = queryString { requestUrl += "&\(_queryString)" }
        
        return requestUrl
    }
}
