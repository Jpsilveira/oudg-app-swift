//
//  HeadersReponse.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 7/25/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import Foundation

/**
 Object with headers response from Restful Model
 */
public struct HeadersResponse {
    
    public var totalCount: Int = 0
    public var next: String? = nil
    public var prev: String? = nil
    public var first: String? = nil
    public var last: String? = nil
    
    public init(totalCount: Int = 0, next: String? = nil, prev: String? = nil, last: String? = nil, first: String? = nil) {
        self.totalCount = totalCount
        self.next = next
        self.prev = prev
        self.first = first
        self.last = last
    }
    
    // Parse headers Responses from class
    static func parseHeadersResponse(allHeaderFields: [NSObject : AnyObject]?) -> HeadersResponse {
        var headersResponse = HeadersResponse()
        if let link = allHeaderFields?["link"] as? String {
            
            if let match = link.rangeOfString("(?<=<)(.*\n?)(?=>; rel=\"next\")", options: .RegularExpressionSearch) {
                headersResponse.next = link.substringWithRange(match)
            }
            if let match = link.rangeOfString("(?<=<)(.*\n?)(?=>; rel=\"last\")", options: .RegularExpressionSearch) {
                headersResponse.last = link.substringWithRange(match)
            }
            if let match = link.rangeOfString("(?<=<)(.*\n?)(?=>; rel=\"first\")", options: .RegularExpressionSearch) {
                headersResponse.first = link.substringWithRange(match)
            }
            if let match = link.rangeOfString("(?<=<)(.*\n?)(?=>; rel=\"prev\")", options: .RegularExpressionSearch) {
                headersResponse.first = link.substringWithRange(match)
            }
            
        }
        
        if let count = allHeaderFields?["X-Total-Count"] as? String,
            let totalCount = Int(count) {
            headersResponse.totalCount = totalCount
        }
        
        return headersResponse
    }
}
