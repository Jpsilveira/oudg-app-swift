//
//  CasteSection.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/3/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit


// MARK : HEADER

class CasteSection: Section {
    
    var answers: [String: Int] = [String: Int]()
    
    var showResult: Bool = false
    
    var _overrideCaste: Bool = false
    
    override func headerView() -> ListHeader.Type {
        
        if error != nil {
            return ErrorHeader.self
            
        } else if showResult || (!UserModel.sharedInstance.casteName.isEmpty && !_overrideCaste) {
            return CasteResultHeader.self
        }
        return CasteHeader.self
    }
    
    override func headerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        
        if showResult || error != nil || (!UserModel.sharedInstance.casteName.isEmpty && !_overrideCaste) || loadingCell() {
            
            return CGSize(width: collectionWidth, height: collectionHeight * 0.7)
        }
        return CGSize(width: collectionWidth, height: collectionHeight * 0.45)
    }

    override func footerView() -> ListFooter.Type {
        return TestFooter.self
    }
    
    override func footerHeight(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {

        return CGSize(width: collectionWidth, height: collectionHeight * 0.15)
    }
    
    override func collectionCell() -> ListCollectionViewCell.Type {
        return RadioCollectionViewCell.self
    }

    override func cellColumnSpace() -> CGFloat {
        return 5
    }
    
    override func insetForSection() -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    override func cellItemSize(collectionWidth: CGFloat, collectionHeight: CGFloat) -> CGSize {
        
        guard headerItem != nil && error == nil else {
            return CGSize.zero
        }
        
        let columnsCount: CGFloat = 5
        let spacements = insetForSection().right + insetForSection().left + (columnsCount * cellColumnSpace())

        let width: CGFloat = (collectionWidth - spacements) / columnsCount
        
        return CGSize(width: width, height: collectionHeight * 0.25)
    }
    
    override func requestItems() {
        
        guard UserModel.sharedInstance.casteName.isEmpty || _overrideCaste else {return}
        
        OUDGApiClient.sharedInstance.getQuiz(1) { (quiz, error) in
            
            self.error = error
            
            if let _questions = quiz?.questions where _questions.count > 0 && error == nil {
                
                self.headerItem = _questions.first
                self.headerItems = _questions
                self.items = (self.headerItem as? QuestionModel)?.options ?? [AnyObject]()
            } else {
                
                self.items = [AnyObject]()
            }
            
            self.delegate?.reloadSection(self)
        }
    }
    
    func requestResult() {
        
        OUDGApiClient.sharedInstance.getResult(1, answers: answers) { (result, error) in
            
            self.error = error
            self._overrideCaste = false
            
            if let _results = result?.results where _results.count > 0 && error == nil {
                
                self.showResult = true
                self.headerItem = _results.first
                self.headerItems = _results
                
                // Update User
                if let result = _results.first {
                    UserModel.sharedInstance.casteName = result.title
                    UserModel.sharedInstance.casteImage = result.image
                    UserModel.sharedInstance.casteDescription = result.description
                    
                    
                    let home = InstanceFactory.navigationController.viewControllers.filter({$0 is HomeViewController}).first as? HomeViewController
                    home?.setCasteImage()
                }
            }
            self.items = [AnyObject]()
            
            self.delegate?.reloadSection(self)
        }
    }
    
    func clearData() {
        
        showResult = false
        headerItem = nil
        headerItems = []
        answers.removeAll()
        items = [LoadingModel()]
        _overrideCaste = true
        delegate?.reloadSection(self)
    }
}


// MARK : Footer

extension CasteSection {
    
    func showFooterButton() -> Bool {
        
        if error != nil {
            
            return false
        }
        return true
    }
    
    func textFooterButton() -> String {
        
        if error != nil || (!UserModel.sharedInstance.casteName.isEmpty && !_overrideCaste) {
            
            return "Voltar ao Menu"
            
        } else if let _item = headerItem as? QuestionModel,
            let _items = headerItems as? [QuestionModel],
            let index = _items.indexOf({$0.questionId == _item.questionId})
            where index == 0 {
        
            return "Voltar ao Menu"
        }
        
        return ""
    }
    
    func showFooterSecondButton() -> Bool {
        
        if error != nil {
            return false
        }
        
        return true
    }
    
    func textFooterSecondButton() -> String {
        
        if error != nil {
            
            return ""
            
        } else if let _item = headerItem as? QuestionModel,
            let _items = headerItems as? [QuestionModel],
            let index = _items.indexOf({$0.questionId == _item.questionId})
            where index == _items.count - 1 {
            
            return "Concluir o Teste"
            
        } else if !UserModel.sharedInstance.casteName.isEmpty && !_overrideCaste {
            
            return "Refazer o Teste"
        }
        
        return ""
    }
    
    func didPressFooterButton(item: AnyObject?) -> Bool {
        
        let index = (headerItems as? [QuestionModel])?.indexOf({$0.questionId == (item as? QuestionModel)?.questionId }) ?? 0
        
        if index == 0 && headerItem is QuestionModel {
        
            delegate?.didPressHeader?(self)
            
            return true
        
        } else if headerItem is QuestionModel && headerItems.count > (index - 1), let _headerItem = headerItems[index - 1] as? QuestionModel {
            
            headerItem = _headerItem
            items = _headerItem.options ?? [AnyObject]()
            delegate?.reloadSection(self)
            return true
        
        } else {
            
            delegate?.didPressHeader?(self)
            return true
        }
    }
    
    func didPressFooterSecondButton(item: AnyObject?) -> Bool {
        
        let index = (headerItems as? [QuestionModel])?.indexOf({$0.questionId == (item as? QuestionModel)?.questionId }) ?? 0
        
        if let _headerItem = headerItem as? QuestionModel
            where answers["\(_headerItem.questionId)"] == nil {
            
            return false
            
        } else if headerItem is QuestionModel && headerItems.count > (index + 1), let _headerItem = headerItems[index + 1] as? QuestionModel {
            
            headerItem = _headerItem
            items = _headerItem.options ?? [AnyObject]()
            delegate?.reloadSection(self)
            
            return true
        
        } else if headerItems.count == (index + 1) {
    
            requestResult()
            return true
        
        } else {
            
            UIAlertController.showAlert(
                title: "Galaniel disse...",
                message: "Você já fez o seu teste no Salão de armas da Cidadela!\n\nTem certeza que deseja refazê-lo?",
                cancelButtonTitle: "Não",
                confirmationButtonTitle: "Sim",
                dissmissBlock: {
                    
                    self.clearData()
                    self.requestItems()
                }
            )

            return true
        }
    }
    
    func didPressHeaderReload(sender: AnyObject) {
        
        error = nil
        
        if headerItems.count > 0  {
            requestResult()
        } else {
            requestItems()
        }
    }
}
