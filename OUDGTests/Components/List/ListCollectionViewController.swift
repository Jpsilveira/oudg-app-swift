//
//  CollectionViewController.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 10/27/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit
import UIView_FrameEx


class ListCollectionViewController: UIViewController {
    
    @IBOutlet weak var collectionView: VHCollectionView!
    
    var sections = [Section]() {
        
        didSet {
            configure()
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.vhDelegate = self
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        
        collectionView.reloadData()
    }
    
    internal func reloadCollectionViewSectionIndex(section: Int, completion: (() -> Void)?) {
        
        executeOnMainQueue {
            self.collectionView.reloadItemsInSection(section, completion: completion)
        }
    }
    
    // MARK: - Private Methods
    
    private func configure() {
        for section in sections {
            
            section.delegate = self
            section.requestItems()
            
            registerHeader(section)
            registerFooter(section)
            registerCell(section)
        }
    }
    
    private func registerHeader(section: Section) {
        
        let header = section.headerView()
        let nib = UINib(nibName: header.defaultReuseIdentifier, bundle: nil)
        
        collectionView.registerNib(nib, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: header.defaultReuseIdentifier)
    }
    
    private func registerFooter(section: Section) {
        
        let footer = section.footerView()
        let nib = UINib(nibName: footer.defaultReuseIdentifier, bundle: nil)
        
        collectionView.registerNib(nib, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footer.defaultReuseIdentifier)
    }
    
    private func registerCell(section: Section) {
        
        let cellClass = section.collectionCell()
        collectionView.registerClass(cellClass, forCellWithReuseIdentifier: cellClass.defaultReuseIdentifier)
        
        let nib = UINib(nibName: cellClass.defaultReuseIdentifier, bundle: nil)
        collectionView.registerNib(nib, forCellWithReuseIdentifier: cellClass.defaultReuseIdentifier)
    }
}

// MARK : HEADER

extension ListCollectionViewController: VHCollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeader section: Int) -> CGSize {
        
        return sections[section].headerHeight(collectionView.width, collectionHeight: collectionView.height)
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooter section: Int) -> CGSize {
        
        return sections[section].footerHeight(collectionView.width, collectionHeight: collectionView.height)
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElement kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        let section = sections[indexPath.section]
        
        switch kind {

        case UICollectionElementKindSectionHeader:
            
            registerHeader(section)
            let reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: section.headerView().defaultReuseIdentifier, forIndexPath: indexPath) as! ListHeader
            
            reusableView.section = section
            reusableView.delegate = section
            reusableView.backgroundColor = section.backgroundColor()
            reusableView.configure(indexPath)
            reusableView.setNeedsLayout()
            reusableView.setNeedsDisplay()
            
            return reusableView
            
        case UICollectionElementKindSectionFooter:
            
            let reusableView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: section.footerView().defaultReuseIdentifier, forIndexPath: indexPath) as! ListFooter
            
            reusableView.section = section
            reusableView.delegate = section
            reusableView.backgroundColor = section.backgroundColor()
            reusableView.configure(indexPath)
            reusableView.setNeedsLayout()
            reusableView.setNeedsDisplay()
            
            return reusableView
            
        default:
            return UICollectionReusableView()
        }
    }
}

// MARK : CELLS

extension ListCollectionViewController {
    
    func numberOfSections(collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sections[section].items.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let section = sections[indexPath.section]
        let identifier = section.collectionCell().defaultReuseIdentifier
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: indexPath) as! ListCollectionViewCell
        
        cell.section = section
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! ListCollectionViewCell
        cell.didSelectCell()
        
        sections[indexPath.section].didSelectItem(indexPath.row)
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        
        guard let cell = cell as? ListCollectionViewCell else { return }
        
        autoreleasepool({
            cell.configure(sections[indexPath.section].items[indexPath.row])
        })
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSection section: Int) -> UIEdgeInsets {
        
        if collectionView.numberOfItemsInSection(section) == 0 {
            return UIEdgeInsetsZero
        }
        
        return sections[section].insetForSection()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSection section: Int) -> CGFloat {
        
        return sections[section].cellColumnSpace() ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSection section: Int) -> CGFloat {
        
        return sections[section].cellLineSpace() ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItem indexPath: NSIndexPath) -> CGSize {
        
        return sections[indexPath.section].cellItemSize(collectionView.width, collectionHeight: collectionView.height) ?? CGSize.zero
    }
}

// MARK : SectionDelegate

extension ListCollectionViewController : SectionDelegate {
    
    func reloadSection(section: Section) {
        
        if let index = sections.indexOf(section) {
            self.reloadCollectionViewSectionIndex(index, completion: nil)
        }
    }
}
