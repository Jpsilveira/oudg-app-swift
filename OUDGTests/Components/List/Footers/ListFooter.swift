//
//  ListFooter.swift
//  OUDGTests
//
//  Created by João Paulo Silveira on 11/5/16.
//  Copyright © 2016 Bitsoft. All rights reserved.
//

import UIKit

@objc protocol ListFooterDelegate: class {
    optional func showFooterButton() -> Bool
    optional func showFooterSecondButton() -> Bool
    optional func textFooterButton() -> String
    optional func textFooterSecondButton() -> String
    optional func didPressFooterButton(item: AnyObject?) -> Bool
    optional func didPressFooterSecondButton(item: AnyObject?) -> Bool
}

public class ListFooter: UICollectionReusableView, UpdateConstraintsProtocol {
    
    @IBOutlet weak var button: UIButton?
    
    @IBOutlet weak var secondButton: UIButton?
    
    weak var delegate: ListFooterDelegate?
    
    var section: Section!
    
    var item: AnyObject!
    
    override public func drawRect(rect: CGRect) {
        super.drawRect(rect)
        setNeedsUpdateSubviewsConstraints()
    }
    
    override public func layoutSubviews() {
        setNeedsUpdateSubviewsConstraints()
        super.layoutSubviews()
    }
    
    func configure(indexPath: NSIndexPath) {}
    
    @IBAction func button(sender: AnyObject) {
        delegate?.didPressFooterButton?(item)
    }
    
    @IBAction func secondButton(sender: AnyObject) {
        delegate?.didPressFooterSecondButton?(item)
    }
}
